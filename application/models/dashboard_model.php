<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard_model extends CI_Model
{
	function get_data_angsuran(){
        $query = $this->db->query("SELECT TAHUN_PAJAK, COUNT(NOPEL) AS JML_ANG FROM PERIMBANGAN.ANGSURAN_PBB GROUP BY TAHUN_PAJAK");
          
        if($query->num_rows() > 0){
            foreach($query->result() as $data){
                $hasil[] = $data;
            }
            return $hasil;
        }
    }

    /*	Fungsi Get Jumlah Angsuran 	*/
    function getTotalAngsuran()
    {
        $query = $this->db->get('PERIMBANGAN.ANGSURAN_PBB');
        if($query->num_rows()>0)
	    {
	      return $query->num_rows();
	    }
	    else
	    {
	      return 0;
	    }
    }

    /*	Fungsi Get Jumlah Angsuran 	*/
    function getAngsuranTahunIni()
    {
    	$where = "TAHUN_PAJAK = 2019";
    	$this->db->where($where);
        $query = $this->db->get('PERIMBANGAN.ANGSURAN_PBB');
        if($query->num_rows()>0)
	    {
	      return $query->num_rows();
	    }
	    else
	    {
	      return 0;
	    }
    }
}	