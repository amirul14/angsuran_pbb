<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends CI_Model
{
    /**
     * This function is used to get the user listing count
     * @param string $searchText : This is optional search text
     * @return number $count : This is row count
     */
    function userListingCount($searchText = '')
    {
        $this->db->select('U.USER_ID, U.NAMA, U.TELPON, R.ROLE_NAME');
        $this->db->from('PENAGIHANWEB.USER_APP U');
        $this->db->join('PENAGIHANWEB.ROLES R', 'R.ROLE_ID = U.ROLE_ID','left');
        if(!empty($searchText)) {
            $likeCriteria = "(UPPER(U.NAMA)  LIKE ('%".$searchText."%')
                            OR  UPPER(U.TELPON)  LIKE ('%".$searchText."%'))";
            $this->db->where($likeCriteria);
        }
        // $this->db->where('U.ROLE_ID !=', 1);
        $query = $this->db->get();
        
        return count($query->result());
    }
    
    /**
     * This function is used to get the user listing count
     * @param string $searchText : This is optional search text
     * @param number $page : This is pagination offset
     * @param number $segment : This is pagination limit
     * @return array $result : This is result
     */
    function userListing($searchText = '', $page, $segment)
    {
        $this->db->select('U.USER_ID, U.NAMA, U.USER_NAME, U.TELPON, R.ROLE_NAME');
        $this->db->from('PENAGIHANWEB.USER_APP U');
        $this->db->join('PENAGIHANWEB.ROLES R', 'R.ROLE_ID = U.ROLE_ID','left');
        if(!empty($searchText)) {
            $likeCriteria = "(UPPER(U.NAMA)  LIKE UPPER('%".$searchText."%')
                            OR  UPPER(U.USER_NAME)  LIKE UPPER('%".$searchText."%')
                            OR  UPPER(U.TELPON)  LIKE UPPER('%".$searchText."%'))";
            $this->db->where($likeCriteria);
        }
        // $this->db->where('U.ROLE_ID !=', 1);
        $this->db->limit($page, $segment);
        $query = $this->db->get();
        // print_r($query->result());
        $result = $query->result();        
        return $result;
    }
    
    /**
     * This function is used to get the user roles information
     * @return array $result : This is result of the query
     */
    function getUserRoles()
    {
        $this->db->select('ROLE_ID, ROLE_NAME');
        $this->db->from('PENAGIHANWEB.ROLES');
        // $this->db->where('ROLE_ID !=', 1);
        $query = $this->db->get();
        
        return $query->result();
    }

    /**
     * This function is used to check whether email id is already exist or not
     * @param {string} $email : This is email id
     * @param {number} $userId : This is user id
     * @return {mixed} $result : This is searched result
     */
    function checkEmailExists($email, $userId = 0)
    {
        $this->db->select("email");
        $this->db->from("tbl_users");
        $this->db->where("email", $email);   
        $this->db->where("isDeleted", 0);
        if($userId != 0){
            $this->db->where("userId !=", $userId);
        }
        $query = $this->db->get();

        return $query->result();
    }
    
    
    /**
     * This function is used to add new user to system
     * @return number $insert_id : This is last inserted id
     */
    function addNewUser($userInfo)
    {
        $this->db->trans_start();
        $this->db->insert('PENAGIHANWEB.USER_APP', $userInfo);
        
        // $insert_id = $this->db->insert_id();
        
        $this->db->trans_complete();
        
        return $insert_id;
    }
    
    /**
     * This function used to get user information by id
     * @param number $userId : This is user id
     * @return array $result : This is user information
     */
    function getUserInfo($userId)
    {
        $this->db->select('USER_ID, USER_NAME, NAMA, TELPON, ROLE_ID');
        $this->db->from('PENAGIHANWEB.USER_APP');
		// $this->db->where('ROLE_ID !=', 1);
        $this->db->where('USER_ID', $userId);
        $query = $this->db->get();
        
        return $query->result();
    }
    
    
    /**
     * This function is used to update the user information
     * @param array $userInfo : This is users updated information
     * @param number $userId : This is user id
     */
    function editUser($userInfo, $userId)
    {
        $this->db->where('USER_ID', $userId);
        $this->db->update('PENAGIHANWEB.USER_APP', $userInfo);
        
        return TRUE;
    }
    
    
    
    /**
     * This function is used to delete the user information
     * @param number $userId : This is user id
     * @return boolean $result : TRUE / FALSE
     */
    function deleteUser($userId)
    {
        $this->db->where('USER_ID', $userId);
        $this->db->delete('PENAGIHANWEB.USER_APP');
        
        return $this->db->affected_rows();
    }


    /**
     * This function is used to match users password for change password
     * @param number $userId : This is user id
     */
    function matchOldPassword($userId, $oldPassword)
    {
        // $this->db->select('USER_ID, PASSWORD');
        // $this->db->where('USER_ID', $userId);
        // $query = $this->db->get('PENAGIHANWEB.USER_APP');
        
        // $user = $query->result();
        $sql = "SELECT USER_ID, PASSWORD FROM PENAGIHANWEB.USER_APP WHERE USER_ID = '$userId' ";

        $query = $this->db->query($sql);

        $user = $query->row_array();
        
        if(!empty($user)){
            if(verifyHashedPassword($oldPassword, $user['PASSWORD'])){
                return $user;
            } else {
                return array();
            }
        } else {
            return array();
        }
    }
    
    /**
     * This function is used to change users password
     * @param number $userId : This is user id
     * @param array $userInfo : This is user updation info
     */
    function changePassword($userId, $userInfo)
    {
        $this->db->where('USER_ID', $userId);
        $this->db->update('PENAGIHANWEB.USER_APP', $userInfo);
        
        return $this->db->affected_rows();
    }
}

  