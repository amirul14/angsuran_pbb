<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class AngsuranPBB_model extends CI_Model
{
	/**
     * This function is used to get the user listing count
     * @param string $searchText : This is optional search text
     * @return number $count : This is row count
     */
    function angsuranpbbListingCount($searchText = '')
    {
        $this->db->select('A.*');
        $this->db->from('PERIMBANGAN.ANGSURAN_PBB A');
        // // $this->db->join('PENAGIHANWEB.ROLES R', 'R.ROLE_ID = U.ROLE_ID','left');
        if(!empty($searchText)) {
            $likeCriteria = "(A.NAMA  LIKE '%".$searchText."%'
                             OR  A.ALAMAT  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by('A.NOPEL','desc');

        // $sql = "SELECT A.*, A.KEC||'.'||A.KEL||'.'||A.BLOK||'.'||A.URUT||'.'||A.JNS AS NOP
        // 		FROM PERIMBANGAN.ANGSURAN_PBB A ";
		// if(!empty($searchText)){
		// 	$sql.= "WHERE A.NAMA  LIKE '%".$searchText."%' OR  A.ALAMAT  LIKE '%".$searchText."%'";
		// }
        
        $query = $this->db->get();
        // $result = $this->db->query($sql)->row_array();
        
        return count($query->result());
    }

    /**
     * This function is used to get the user listing count
     * @param string $searchText : This is optional search text
     * @param number $page : This is pagination offset
     * @param number $segment : This is pagination limit
     * @return array $result : This is result
     */
    function angsuranpbbListing($searchText = '', $page, $segment)
    {
        $this->db->select('A.*');
        $this->db->from('PERIMBANGAN.ANGSURAN_PBB A');
        // // $this->db->join('PENAGIHANWEB.ROLES R', 'R.ROLE_ID = U.ROLE_ID','left');
        if(!empty($searchText)) {
            $likeCriteria = "(UPPER(A.NAMA)  LIKE UPPER('%".$searchText."%')
                             OR UPPER(A.NOPEL)  LIKE UPPER('%".$searchText."%')
                             OR  UPPER(A.ALAMAT)  LIKE UPPER('%".$searchText."%'))";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by('A.NOPEL','desc');
        
    	// $sql = "SELECT A.*, A.KEC||'.'||A.KEL||'.'||A.BLOK||'.'||A.URUT||'.'||A.JNS AS NOP
     //    		FROM PERIMBANGAN.ANGSURAN_PBB A ";
		// if(!empty($searchText)){
		// 	$sql.= "WHERE A.NAMA  LIKE '%".$searchText."%' OR  A.ALAMAT  LIKE '%".$searchText."%'";
		// }

        $this->db->limit($page, $segment);
        $query = $this->db->get();
        
        $result = $query->result();        
        return $result;
    }

    /**
     * This function is used to get the user roles information
     * @return array $result : This is result of the query
     */
    function getAngsuran()
    {
        $this->db->select('*');
        $this->db->from('PERIMBANGAN.ANGSURAN_PBB');
        // $this->db->where('ROLE_ID !=', 1);
        $query = $this->db->get();
        
        return $query->result();
    }

    /**
     * This function is used to get the user roles information
     * @return array $result : This is result of the query
     */
    function getWPInfo($nop,$thnpajak)
    {
    	list($prop, $dati, $kec, $kel, $blok, $urut, $jns) = explode('.', $nop);
        
   //      $q = "SELECT   A.T_KEC_KD,
		 //         A.T_KEL_KD,
		 //         A.D_NOP_BLK,
		 //         A.D_NOP_URUT,
		 //         A.D_NOP_JNS,
		 //         D_WP_NAMA,
		 //         D_OP_JLN || ' NO. ' || D_OP_JLNO ALAMATOP,
		 //         D_PJK_TAX - NVL (D_PJK_ADJ, 0) POKOK,
		 //         D_PJK_TAX,
		 //         D_PJK_ADJ,
		 //         TO_CHAR(D_PJK_TJTT, 'MM/DD/YYYY') JTEMPO,
		 //         DENDA_DISETUJUI
		 //  FROM      DATAOP@LIHATGATOTKACA A
		 //         JOIN
		 //            DATABAYAR@LIHATGATOTKACA B
		 //         ON     A.T_KEC_KD = B.T_KEC_KD
		 //            AND A.T_KEL_KD = B.T_KEL_KD
		 //            AND A.D_NOP_BLK = B.D_NOP_BLK
		 //            AND A.D_NOP_URUT = B.D_NOP_URUT
		 // WHERE       A.T_KEC_KD = '$kec'
		 //         AND A.T_KEL_KD = '$kel'
		 //         AND A.D_NOP_BLK = '$blok'
		 //         AND A.D_NOP_URUT = '$urut'
		 //         AND A.D_NOP_JNS = '$jns'
		 //         AND D_PJK_THN = $thnpajak ";

        $q = "SELECT   A.T_KEC_KD,
                 A.T_KEL_KD,
                 A.D_NOP_BLK,
                 A.D_NOP_URUT,
                 A.D_NOP_JNS,
                 B.D_PJK_THN,
                 D_WP_NAMA,
                 D_OP_JLN || ' NO. ' || D_OP_JLNO ALAMATOP,
                 (D_PJK_TAX - NVL (D_PJK_ADJ, 0) - REALPOKOK) POKOK,
                 D_PJK_TAX,
                 D_PJK_ADJ,
                 TO_CHAR(D_PJK_TJTT, 'MM/DD/YYYY') JTEMPO,
                 DECODE (
                    DENDA_DISETUJUI,
                    NULL,
                    PERIMBANGAN.FuncHitungDendanew2 (
                       SYSDATE,
                       D_PJK_TJTT,
                       (D_PJK_TAX - NVL (D_PJK_ADJ, 0) - REALPOKOK),
                       0
                    ),
                    DENDA_DISETUJUI
                 )
                    SANKSI
          FROM         DATAOP@LIHATGATOTKACA A
                    JOIN
                       DATABAYAR@LIHATGATOTKACA B
                    ON     A.T_KEC_KD = B.T_KEC_KD
                       AND A.T_KEL_KD = B.T_KEL_KD
                       AND A.D_NOP_BLK = B.D_NOP_BLK
                       AND A.D_NOP_URUT = B.D_NOP_URUT
                 LEFT JOIN
                    (  SELECT   T_KEC_KD,
                                T_KEL_KD,
                                D_NOP_BLK,
                                D_NOP_URUT,
                                D_NOP_JNS,
                                D_PJK_THN,
                                SUM (D_PJK_PBB) REALPOKOK
                         FROM   PERIMBANGAN.REALISASI_NONREKON
                     GROUP BY   T_KEC_KD,
                                T_KEL_KD,
                                D_NOP_BLK,
                                D_NOP_URUT,
                                D_NOP_JNS,
                                D_PJK_THN) C
                 ON     B.T_KEC_KD = C.T_KEC_KD
                    AND B.T_KEL_KD = C.T_KEL_KD
                    AND B.D_NOP_BLK = C.D_NOP_BLK
                    AND B.D_NOP_URUT = C.D_NOP_URUT
                    AND B.D_PJK_THN = C.D_PJK_THN
         WHERE       A.T_KEC_KD = '$kec'
                 AND A.T_KEL_KD = '$kel'
                 AND A.D_NOP_BLK = '$blok'
                 AND A.D_NOP_URUT = '$urut'
                 AND A.D_NOP_JNS = '$jns'
                 AND B.D_PJK_THN = $thnpajak ";

		$result = $this->db->query($q)->row_array();

		$a_data['namawp'] = $result['D_WP_NAMA'];
		$a_data['alamatwp'] = $result['ALAMATOP'];
		$a_data['pokok'] = $result['POKOK'];
		$a_data['jtempo'] = $result['JTEMPO'];
		$a_data['adj'] = $result['D_PJK_ADJ'];
		$a_data['denda'] = $result['SANKSI'];

		return $a_data;
    }

    /**
     * This function is used to add new angsuran to system
     * @return number $insert_id : This is last inserted id
     */
    function addNewAngsuran($angsuranInfo)
    {
        // list($prop, $dati, $kec, $kel, $blok, $urut, $jns) = explode('-', $angsuranInfo['nop']);

        // if(empty($angsuranInfo['denda']))
        //     $denda = 0;
        // else
        //     $denda = $angsuranInfo['denda'];

        // $a_data['TGL_PENGAJUAN']        = $angsuranInfo['tglpengajuan'];
        // $a_data['NAMA']                 = $angsuranInfo['nama'];
        // $a_data['ALAMAT']               = $angsuranInfo['alamat'];
        // $a_data['NIK']                  = $angsuranInfo['nik'];
        // $a_data['TELP']                 = $angsuranInfo['telp'];
        // $a_data['KEC']                  = $kec;
        // $a_data['KEL']                  = $kel;
        // $a_data['BLOK']                 = $blok;
        // $a_data['URUT']                 = $urut;
        // $a_data['JNS']                  = $jns;
        // $a_data['TAHUN_PAJAK']          = $angsuranInfo['thnpajak'];
        // $a_data['POKOK']                = $angsuranInfo['pokok'];
        // $a_data['ADJ']                  = $angsuranInfo['adj'];
        // // $a_data['JTEMPO']               = $angsuranInfo['jtempo'];
        // $a_data['JTEMPO']               = $angsuranInfo['ins_jtempo'];
        // $a_data['DENDA_DISETUJUI']      = $denda;
        // $a_data['JML_ANGSURAN']         = $angsuranInfo['jmlangsuran'];
        // $a_data['STS_ANGSURAN']         = 0;
        // $a_data['KET']                  = $angsuranInfo['ket'];
        // $a_data['UPD_BY']               = 'ADMIN';

        // foreach ($angsuranInfo as $key => $value) {
        //     if( $key == 'TGL_PENGAJUAN' or $key == 'JTEMPO' )
        //         $this->db->set($key, "to_date('$value', 'YYYY-MM-DD')", false);
        //     else
        //         $this->db->set($key, $value);   
        // }
        
        return $this->db->insert('PERIMBANGAN.ANGSURAN_PBB',$angsuranInfo);
    }

    /**
     * This function used to get angsuran information by nopel
     * @param number $nopel : This is nomor pelayanan
     * @return array $result : This is angsuran information
     */
    function getAngsuranInfo($nopel)
    {
        $this->db->select('*');
        $this->db->from('PERIMBANGAN.ANGSURAN_PBB');
        $this->db->where('NOPEL', $nopel);
        $query = $this->db->get();
        
        return $query->result();
    }

    /**
     * This function used to get angsuran information by nopel
     * @param number $nopel : This is nomor pelayanan
     * @return array $result : This is angsuran information
     */
    function getWPAddress($nopel)
    {
        $sql = "SELECT   B.T_KEC_KD,
                 B.T_KEL_KD,
                 B.D_NOP_BLK,
                 B.D_NOP_URUT,
                 B.D_WP_NAMA,
                 B.D_OP_JLN,
                 B.D_OP_JLNO
          FROM      PERIMBANGAN.ANGSURAN_PBB A
                 JOIN
                    DATAOP@LIHATGATOTKACA B
                 ON     A.KEC = B.T_KEC_KD
                    AND A.KEL = B.T_KEL_KD
                    AND A.BLOK = B.D_NOP_BLK
                    AND A.URUT = B.D_NOP_URUT
         WHERE   NOPEL = ?";

        $query = $this->db->query($sql, $nopel);
        
        return $query->result();
    }


    /**
     * This function is used to update the user information
     * @param array $userInfo : This is users updated information
     * @param number $userId : This is user id
     */
    function editAngsuranVal($angsuranInfo, $detailangsuranInfo, $nopel)
    {
        $a_data['TGL_PENGAJUAN']        = $angsuranInfo['tglpengajuan'];
        $a_data['NAMA']                 = $angsuranInfo['nama'];
        $a_data['ALAMAT']               = $angsuranInfo['alamat'];
        $a_data['NIK']                  = $angsuranInfo['nik'];
        $a_data['TELP']                 = $angsuranInfo['telp'];
        $a_data['JML_ANGSURAN']         = $angsuranInfo['jmlangsuran'];
        $a_data['KET']                  = $angsuranInfo['ket'];

        foreach ($angsuranInfo as $key => $value) {
            if( $key == 'TGL_PENGAJUAN' )
                $this->db->set($key, "to_date('$value', 'YYYY-MM-DD')", false);
            else
                $this->db->set($key, $value);   
        }

        $this->db->where('NOPEL', $nopel);
        $this->db->update('PERIMBANGAN.ANGSURAN_PBB');

        $this->db->where_in('NOPEL', $nopel);
        $this->db->delete('PERIMBANGAN.ANGSURAN_PBB_DET');

        $this->db->insert_batch('PERIMBANGAN.ANGSURAN_PBB_DET',$detailangsuranInfo);
                
        return TRUE;
    }

    function simpanDetAngsuranVal($angsuranInfo, $detailangsuranInfo, $nopel)
    {
        $a_data['TGL_PENGAJUAN']        = $angsuranInfo['tglpengajuan'];
        $a_data['NAMA']                 = $angsuranInfo['nama'];
        $a_data['ALAMAT']               = $angsuranInfo['alamat'];
        $a_data['NIK']                  = $angsuranInfo['nik'];
        $a_data['TELP']                 = $angsuranInfo['telp'];
        $a_data['JML_ANGSURAN']         = $angsuranInfo['jmlangsuran'];
        $a_data['KET']                  = $angsuranInfo['ket'];

        foreach ($angsuranInfo as $key => $value) {
            if( $key == 'TGL_PENGAJUAN' )
                $this->db->set($key, "to_date('$value', 'YYYY-MM-DD')", false);
            else
                $this->db->set($key, $value);   
        }

        $this->db->where('NOPEL', $nopel);
        $this->db->update('PERIMBANGAN.ANGSURAN_PBB');

        $this->db->insert_batch('PERIMBANGAN.ANGSURAN_PBB_DET',$detailangsuranInfo);

        return TRUE;
    }


    /**
     * This function used to get angsuran information by nopel
     * @param number $nopel : This is nomor pelayanan
     * @return array $result : This is angsuran information
     */
    function getAngsuranDetail($nopel)
    {
        $this->db->select('*');
        $this->db->from('PERIMBANGAN.ANGSURAN_PBB_DET');
        $this->db->where('NOPEL', $nopel);
        $query = $this->db->get();
        
        return $query->result();
    }


    function getMaxNopel()
    {
        $this->db->select_max('NOPEL');
        $this->db->from('PERIMBANGAN.ANGSURAN_PBB');
        
        return $this->db->get()->row()->NOPEL;
    }

}	