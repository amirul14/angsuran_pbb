<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

/**
 * Class : Dashboard (Dashboard Controller)
 * Dashboard Class to control all user related operations.
 * @author : Kishor Mali
 * @version : 1.1
 * @since : 15 November 2016
 */
class Dashboard extends BaseController
{
	/**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('dashboard_model');
        $this->isLoggedIn();   
    }


    /**
     * This function used to load the first screen of the user
     */
    public function index()
    {
        $this->global['pageTitle'] = 'Penagihan  : Dashboard';
        
        $data['data_angsuran'] = $this->dashboard_model->get_data_angsuran();
        $data['jml_ang'] = $this->dashboard_model->getTotalAngsuran();
        $data['jml_ang_thn_ini'] = $this->dashboard_model->getAngsuranTahunIni();
        // print_r($this->db->last_query());die();
        $this->loadViews("dashboard", $this->global, $data , NULL);
    }


}	