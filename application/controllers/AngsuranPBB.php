<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

/**
 * Class : User (UserController)
 * User Class to control all user related operations.
 * @author : Kishor Mali
 * @version : 1.1
 * @since : 15 November 2016
 */
class AngsuranPBB extends BaseController
{

	/**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->library('pdf');
        $this->load->helper('url');
        $this->load->model('angsuranpbb_model');
        $this->isLoggedIn();
    }

    /**
     * This function used to load the first screen of the user
     */
    public function index()
    {
        $this->global['pageTitle'] = 'Penagihan  : Dashboard';
        
        $this->loadViews("dashboard", $this->global, NULL , NULL);
    }

    /**
     * This function is used to load the user list
     */
    function angsuranpbbListing()
    {
        if($this->isEditor() == FALSE)
        {
            $this->loadThis();
            // echo "isAdmin";die();
        }
        else
        {
            $this->load->model('angsuranpbb_model');
        
            $searchText = $this->input->post('searchText');
            $data['searchText'] = $searchText;
            
            $this->load->library('pagination');
            
            $count = $this->angsuranpbb_model->angsuranpbbListingCount($searchText);

			$returns = $this->paginationCompress ( "angsuranpbbListing/", $count, 5 );
            
            $data['angsuranpbbRecords'] = $this->angsuranpbb_model->angsuranpbbListing($searchText, $returns["page"], $returns["segment"]);
            
            $this->global['pageTitle'] = 'Penagihan  : Angsuran PBB Listing';
            
            $this->loadViews("angsuranpbb", $this->global, $data, NULL);
        }
    }

    /**
     * This function is used to load the add new form
     */
    function addNewAngPBB()
    {
        if($this->isEditor() == FALSE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->model('angsuranpbb_model');
            $data['roles'] = $this->angsuranpbb_model->getAngsuran();
            
            $this->global['pageTitle'] = 'Penagihan  : Tambah Angsuran Baru';

            $this->loadViews("addNewAngPBB", $this->global, $data, NULL);
        }
    }

    /**
     * Function ini digunakan mencari informasi Data Wajib Pajak dan Pokok Terutang
     */
    public function cariNOP()
    {
		$this->load->model('angsuranpbb_model');

		$post = $this->input->post();

		$dataWP = $this->angsuranpbb_model->getWPInfo($post['nop'],$post['thnnop']);
        // print_r($this->db->last_query());die();
		echo json_encode($dataWP);
	}


	/**
     * This function is used to add new user to the system
     */
    function addNewAngsuran()
    {
        if($this->isEditor() == FALSE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->library('form_validation');
            
            $this->form_validation->set_rules('nop','Nomor Objek Pajak','trim|required|max_length[128]|xss_clean');
            $this->form_validation->set_rules('thnpajak','Tahun Pajak','trim|required|numeric|xss_clean');
            $this->form_validation->set_rules('nama','Nama Pemohon','required|max_length[128]');
            $this->form_validation->set_rules('alamat','Alamat Pemohon','required|max_length[128]');
            $this->form_validation->set_rules('nik','Nomor Induk Kependudukan (NIK)','trim|required|numeric');
            $this->form_validation->set_rules('telp','Nomor Telpon','required|max_length[15]|xss_clean|numeric');
            $this->form_validation->set_rules('tglpengajuan','Tanggal Pengajuan','required');
            $this->form_validation->set_rules('jmlangsuran','Jumlah Angsuran','required|numeric');
            $this->form_validation->set_rules('ket','Keterangan');
            
            if($this->form_validation->run() == FALSE)
            {
                $this->addNewAngPBB();
            }
            else
            {
                $nop = $this->input->post('nop');
                $thnpajak = $this->input->post('thnpajak');
                $nama = $this->input->post('nama');
                $alamat = $this->input->post('alamat');
                $nik = $this->input->post('nik');
                $telp = $this->input->post('telp');
                $tglpengajuan = $this->input->post('tglpengajuan');
                $jmlangsuran = $this->input->post('jmlangsuran');
                $ket = $this->input->post('ket');
                $pokok = $this->input->post('pokok');
                $adj = $this->input->post('adj');
                $denda = $this->input->post('denda');
                $jtempo = $this->input->post('jtempo');
                
                echo 'Ini inputan Jatuh Tempo '.$jtempo;
                
                // $new_jtempo = str_replace('/', '-', $jtempo );
                $ins_jtempo = date("Y-m-d", strtotime($jtempo));
                
                // echo 'Ini inputan Jatuh Tempo After Editing '.$ins_jtempo;die();

                list($prop, $dati, $kec, $kel, $blok, $urut, $jns) = explode('.', $nop);

                $angsuranInfo = array('KEC'=>$kec, 
                                        'KEL'                   =>$kel, 
                                        'BLOK'                  =>$blok, 
                                        'URUT'                  =>$urut, 
                                        'JNS'                   =>$jns, 
                                        'TAHUN_PAJAK'           =>$thnpajak, 
                                        'NAMA'                  =>$nama, 
                                        'ALAMAT'                =>$alamat, 
                                        'NIK'                   =>$nik, 
                                        'TELP'                  =>$telp, 
                                        'JML_ANGSURAN'          =>$jmlangsuran, 
                                        'KET'                   =>$ket, 
                                        'POKOK'                 =>$pokok, 
                                        'ADJ'                   =>$adj, 
                                        'DENDA_DISETUJUI'       =>empty($denda) ? 0 : $denda,
                                        'TGL_PENGAJUAN'         =>date_format(date_create($tglpengajuan),"d-M-Y"),
                                        'JTEMPO'                =>date_format(date_create($ins_jtempo),"d-M-Y"),
                                        'STS_ANGSURAN'          =>0,
                                        'UPD_BY'                =>$this->name, 
                                        'INS_BY'                =>$this->name);

                $this->load->model('angsuranpbb_model');
                $result = $this->angsuranpbb_model->addNewAngsuran($angsuranInfo);
                // print_r($this->db->last_query());die();
                if($result > 0)
                {
                    $this->session->set_flashdata('success', 'Angsuran Baru berhasil ditambahkan');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Angsuran Baru gagal ditambahkan');
                }
                
                $max_nopel = $this->angsuranpbb_model->getMaxNopel();
                
                redirect('simpanDetAngsuran/'.$max_nopel);
            }
        }
    }

    /**
     * This function is used load user edit information
     * @param number $userId : Optional : This is user id
     */
    function editOldAngsuran($nopel = NULL)
    {   
        if($this->isEditor() == FALSE)
        {
            $this->loadThis();
        }
        else
        {
            if($nopel == null)
            {
                redirect('angsuranpbbListing');
            }
            
            $data['address'] = $this->angsuranpbb_model->getWPAddress($nopel);
            $data['angsuranpbbInfo'] = $this->angsuranpbb_model->getAngsuranInfo($nopel);
            $data['angsuranpbbDetail'] = $this->angsuranpbb_model->getAngsuranDetail($nopel);
            
            $this->global['pageTitle'] = 'Penagihan  : Sunting Angsuran PBB';
            // var_dump($data);die();
            $this->loadViews("editOldAngsuran", $this->global, $data, NULL);
        }
    }

    function getData($nopel = NULL)
    {
        if($nopel == null)
        {
            redirect('angsuranpbbListing');
        }

        $data['angsuranpbbInfo'] = $this->angsuranpbb_model->getAngsuranInfo($nopel);
    }

    /**
     * This function is used to edit the user information
     */
    function editAngsuran()
    {
        if($this->isEditor() == FALSE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->library('form_validation');
            
            $nopel = $this->input->post('nopel');
            
            $this->form_validation->set_rules('nama','Nama Pemohon','required|max_length[128]');
            $this->form_validation->set_rules('alamat','Alamat Pemohon','required|max_length[128]');
            $this->form_validation->set_rules('nik','Nomor Induk Kependudukan (NIK)','trim|required|numeric');
            $this->form_validation->set_rules('telp','Nomor Telpon','required|max_length[15]|xss_clean|numeric');
            $this->form_validation->set_rules('tglpengajuan','Tanggal Pengajuan','required');
            $this->form_validation->set_rules('jmlangsuran','Jumlah Angsuran','required|numeric');
            $this->form_validation->set_rules('ket','Keterangan');

            $this->form_validation->set_rules('angke[]','Angsuran Ke','required|numeric');
            $this->form_validation->set_rules('pokok_det[]','Pokok Angsuran','required|numeric');
            $this->form_validation->set_rules('sanksi_det[]','Sanksi Angsuran','required|numeric');
            $this->form_validation->set_rules('tglbayar[]','Tanggal Janji Bayar','required');
            
            // $this->form_validation->set_rules('tot_angsuran','Total Angsuran','matches[tot_tunggakan]');

            if($this->form_validation->run() == FALSE)
            {
                $this->editOldAngsuran($nopel);
            }
            else
            {
                $nama = $this->input->post('nama');
                $alamat = $this->input->post('alamat');
                $nik = $this->input->post('nik');
                $telp = $this->input->post('telp');
                $tglpengajuan = $this->input->post('tglpengajuan');
                $jmlangsuran = $this->input->post('jmlangsuran');
                $ket = $this->input->post('ket');

                $ang_ke = $this->input->post('angke[]');
                $pokok_det = $this->input->post('pokok_det[]');
                $sanksi_det = $this->input->post('sanksi_det[]');
                $tglbayar = $this->input->post('tglbayar[]');
                
                $tot_tunggakan = $this->input->post('tot_tunggakan');
                $tot_angsuran = $this->input->post('tot_angsuran');

                $angsuranInfo = array();
                
                    $angsuranInfo = array('NAMA'=>$nama, 'ALAMAT'=>$alamat, 'NIK'=> $nik, 'TELP'=>$telp, 'JML_ANGSURAN'=>$jmlangsuran, 'KET'=>$ket, 'TGL_PENGAJUAN'=>$tglpengajuan, 'UPD_BY'=>$this->name);

                $detailangsuranInfo = array();
                $total_post = count($ang_ke);

                foreach ($ang_ke as $key => $value) {
                    $detailangsuranInfo[] = array(
                                            'NOPEL'             => $nopel,
                                            'ANGS_KE'           => $ang_ke[$key],
                                            'POKOK_ANGS'        => $pokok_det[$key],
                                            'SANKSI_ANGS'       => $sanksi_det[$key],
                                            'INS_BY'            => $this->name,
                                            'TGL_JANJI_BYR'     => date_format(date_create($tglbayar[$key]),"d-M-Y")
                                            );
                }

                if( $tot_angsuran < $tot_tunggakan ) {
                    
                    $this->session->set_flashdata('error', 'Total Angsuran yang dimasukkan lebih kecil dari Total Tunggakan');   

                    redirect('editOldAngsuran/'.$nopel);

                } else if ( $tot_angsuran > $tot_tunggakan ) {
                    
                    $this->session->set_flashdata('error', 'Total Angsuran yang dimasukkan lebih besar dari Total Tunggakan');

                    redirect('editOldAngsuran/'.$nopel);

                } else {

                    $result = $this->angsuranpbb_model->editAngsuranVal($angsuranInfo, $detailangsuranInfo, $nopel);
                    // print_r($this->db->last_query());die();
                    if($result == true)
                    {
                        $this->session->set_flashdata('success', 'Angsuran PBB berhasil dirubah');
                    }
                    else
                    {
                        $this->session->set_flashdata('error', 'Angsuran PBB gagal dirubah');
                    }
                    
                }

                redirect('lihatDetAngsuran/'.$nopel);
                
                // redirect('angsuranpbbListing');
            }
        }
    }

    /**
     * This function is used load user edit information
     * @param number $userId : Optional : This is user id
     */
    function simpanDetAngsuran($nopel = NULL)
    {   
        if($this->isEditor() == FALSE)
        {
            $this->loadThis();
        }
        else
        {
            if($nopel == null)
            {
                redirect('angsuranpbbListing');
            }
            
            $data['address'] = $this->angsuranpbb_model->getWPAddress($nopel);
            $data['angsuranpbbInfo'] = $this->angsuranpbb_model->getAngsuranInfo($nopel);
            
            $this->global['pageTitle'] = 'Penagihan  : Sunting Detail Angsuran PBB';
            
            $this->loadViews("simpanDetAngsuran", $this->global, $data, NULL);
        }
    }

    function simpanDetailAngsuran()
    {
        if($this->isEditor() == FALSE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->library('form_validation');
            
            $nopel = $this->input->post('nopel');

            $this->form_validation->set_rules('nama','Nama Pemohon','required|max_length[128]');
            $this->form_validation->set_rules('alamat','Alamat Pemohon','required|max_length[128]');
            $this->form_validation->set_rules('nik','Nomor Induk Kependudukan (NIK)','trim|required|numeric');
            $this->form_validation->set_rules('telp','Nomor Telpon','required|max_length[15]|xss_clean|numeric');
            $this->form_validation->set_rules('tglpengajuan','Tanggal Pengajuan','required');
            $this->form_validation->set_rules('jmlangsuran','Jumlah Angsuran','required|numeric');
            $this->form_validation->set_rules('ket','Keterangan');
            
            $this->form_validation->set_rules('angke[]','Angsuran Ke','required|numeric');
            $this->form_validation->set_rules('pokok_det[]','Pokok Angsuran','required|numeric');
            $this->form_validation->set_rules('sanksi_det[]','Sanksi Angsuran','required|numeric');
            $this->form_validation->set_rules('tglbayar[]','Tanggal Janji Bayar','required');

            if($this->form_validation->run() == FALSE)
            {
                $this->simpanDetAngsuran($nopel);
            }
            else
            {
                $nama = $this->input->post('nama');
                $alamat = $this->input->post('alamat');
                $nik = $this->input->post('nik');
                $telp = $this->input->post('telp');
                $tglpengajuan = $this->input->post('tglpengajuan');
                $jmlangsuran = $this->input->post('jmlangsuran');
                $ket = $this->input->post('ket');

                $ang_ke = $this->input->post('angke[]');
                $pokok_det = $this->input->post('pokok_det[]');
                $sanksi_det = $this->input->post('sanksi_det[]');
                $tglbayar = $this->input->post('tglbayar[]');
                
                $tot_tunggakan = $this->input->post('tot_tunggakan');
                $tot_angsuran = $this->input->post('tot_angsuran');

                $angsuranInfo = array();
                
                    $angsuranInfo = array('NAMA'=>$nama, 'ALAMAT'=>$alamat, 'NIK'=> $nik, 'TELP'=>$telp, 'JML_ANGSURAN'=>$jmlangsuran, 'KET'=>$ket, 'TGL_PENGAJUAN'=>$tglpengajuan, 'UPD_BY'=>$this->name);

                $detailangsuranInfo = array();
                $total_post = count($ang_ke);

                foreach ($ang_ke as $key => $value) {
                    $detailangsuranInfo[] = array(
                                            'NOPEL'             => $nopel,
                                            'ANGS_KE'           => $ang_ke[$key],
                                            'POKOK_ANGS'        => $pokok_det[$key],
                                            'SANKSI_ANGS'       => $sanksi_det[$key],
                                            'INS_BY'            => $this->name,
                                            'TGL_JANJI_BYR'     => date_format(date_create($tglbayar[$key]),"d-M-Y")
                                            );
                }

                if( $tot_angsuran < $tot_tunggakan ) {
                    
                    $this->session->set_flashdata('error', 'Total Angsuran yang dimasukkan lebih kecil dari Total Tunggakan');   

                    redirect('simpanDetAngsuran/'.$nopel);

                } else if ( $tot_angsuran > $tot_tunggakan ) {
                    
                    $this->session->set_flashdata('error', 'Total Angsuran yang dimasukkan lebih besar dari Total Tunggakan');

                    redirect('simpanDetAngsuran/'.$nopel);

                } else {

                    $result = $this->angsuranpbb_model->simpanDetAngsuranVal($angsuranInfo, $detailangsuranInfo, $nopel);
                    // print_r($this->db->last_query());die();
                    if($result == true)
                    {
                        $this->session->set_flashdata('success', 'Detail Angsuran PBB berhasil disimpan');
                    }
                    else
                    {
                        $this->session->set_flashdata('error', 'Detail Angsuran PBB gagal disimpan');
                    }
                    
                }

                redirect('lihatDetAngsuran/'.$nopel);
            }

        }
    }

    function lihatDetAngsuran($nopel = NULL)
    {   
        if($this->isEditor() == FALSE)
        {
            $this->loadThis();
        }
        else
        {
            if($nopel == null)
            {
                redirect('angsuranpbbListing');
            }
            
            $data['address'] = $this->angsuranpbb_model->getWPAddress($nopel);
            $data['angsuranpbbInfo'] = $this->angsuranpbb_model->getAngsuranInfo($nopel);
            $data['angsuranpbbDetail'] = $this->angsuranpbb_model->getAngsuranDetail($nopel);
            
            $this->global['pageTitle'] = 'Penagihan  : Informasi Detail Pengajuan Angsuran PBB';
            
            $this->loadViews("lihatDetAngsuran", $this->global, $data, NULL);
        }
    }

    function cetakSK($nopel = NULL)
    {
        if($this->isEditor() == FALSE)
        {
            $this->loadThis();
        }
        else
        {
            // print_r($nopel);die();
            if($nopel == null)
            {
                redirect('angsuranpbbListing');
            }
            
            $data['address'] = $this->angsuranpbb_model->getWPAddress($nopel);
            $data['angsuranpbbInfo'] = $this->angsuranpbb_model->getAngsuranInfo($nopel);
            $data['angsuranpbbDetail'] = $this->angsuranpbb_model->getAngsuranDetail($nopel);

            $this->loadViews("cetakSK", $this->global, $data, NULL);
        }
    }

}	