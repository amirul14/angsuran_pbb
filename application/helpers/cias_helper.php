<?php if(!defined('BASEPATH')) exit('No direct script access allowed');


/**
 * This function is used to print the content of any data
 */
function pre($data)
{
    echo "<pre>";
    print_r($data);
    echo "</pre>";
}

/**
 * This function used to get the CI instance
 */
if(!function_exists('get_instance'))
{
    function get_instance()
    {
        $CI = &get_instance();
    }
}

/**
 * This function used to generate the hashed password
 * @param {string} $plainPassword : This is plain text password
 */
if(!function_exists('getHashedPassword'))
{
    function getHashedPassword($plainPassword)
    {
        return password_hash($plainPassword, PASSWORD_DEFAULT);
    }
}

/**
 * This function used to generate the hashed password
 * @param {string} $plainPassword : This is plain text password
 * @param {string} $hashedPassword : This is hashed password
 */
if(!function_exists('verifyHashedPassword'))
{
    function verifyHashedPassword($plainPassword, $hashedPassword)
    {
        return password_verify($plainPassword, $hashedPassword) ? true : false;
    }
}

/**
 * This method used to get current browser agent
 */
if(!function_exists('getBrowserAgent'))
{
    function getBrowserAgent()
    {
        $CI = get_instance();
        $CI->load->library('user_agent');

        $agent = '';

        if ($CI->agent->is_browser())
        {
            $agent = $CI->agent->browser().' '.$CI->agent->version();
        }
        else if ($CI->agent->is_robot())
        {
            $agent = $CI->agent->robot();
        }
        else if ($CI->agent->is_mobile())
        {
            $agent = $CI->agent->mobile();
        }
        else
        {
            $agent = 'Unidentified User Agent';
        }

        return $agent;
    }
}

if(!function_exists('setProtocol'))
{
    function setProtocol()
    {
        $CI = &get_instance();
                    
        $CI->load->library('email');
        
        $config['protocol'] = PROTOCOL;
        $config['mailpath'] = MAIL_PATH;
        $config['smtp_host'] = SMTP_HOST;
        $config['smtp_port'] = SMTP_PORT;
        $config['smtp_user'] = SMTP_USER;
        $config['smtp_pass'] = SMTP_PASS;
        $config['charset'] = "utf-8";
        $config['mailtype'] = "html";
        $config['newline'] = "\r\n";
        
        $CI->email->initialize($config);
        
        return $CI;
    }
}

if(!function_exists('emailConfig'))
{
    function emailConfig()
    {
        $CI->load->library('email');
        $config['protocol'] = PROTOCOL;
        $config['smtp_host'] = SMTP_HOST;
        $config['smtp_port'] = SMTP_PORT;
        $config['mailpath'] = MAIL_PATH;
        $config['charset'] = 'UTF-8';
        $config['mailtype'] = "html";
        $config['newline'] = "\r\n";
        $config['wordwrap'] = TRUE;
    }
}

if(!function_exists('resetPasswordEmail'))
{
    function resetPasswordEmail($detail)
    {
        $data["data"] = $detail;
        // pre($detail);
        // die;
        
        $CI = setProtocol();        
        
        $CI->email->from(EMAIL_FROM, FROM_NAME);
        $CI->email->subject("Reset Password");
        $CI->email->message($CI->load->view('email/resetPassword', $data, TRUE));
        $CI->email->to($detail["email"]);
        $status = $CI->email->send();
        
        return $status;
    }
}

if(!function_exists('setFlashData'))
{
    function setFlashData($status, $flashMsg)
    {
        $CI = get_instance();
        $CI->session->set_flashdata($status, $flashMsg);
    }
}

if(!function_exists('formatNumber'))
{
    // mengubah format bilangan
    function formatNumber($num,$dec=0) {
        return number_format($num,$dec,',','.');
    }
}


if(!function_exists('splitDate'))
{
    // mengubah format tanggal dari yyyy-mm-dd menjadi array d m y
    function splitDate($ymd,$dmy=false,$delim='-') {
        if($dmy)
            list($d,$m,$y) = explode($delim,substr($ymd,0,10));
        else
            list($y,$m,$d) = explode($delim,substr($ymd,0,10));
        
        return array($d,$m,$y);
    }
}

if(!function_exists('arrayDay'))
{
    function arrayDay($full=true) {
        $hari = array();
        
        if($full) {
            $hari[1] = 'Senin';
            $hari[2] = 'Selasa';
            $hari[3] = 'Rabu';
            $hari[4] = 'Kamis';
            $hari[5] = 'Jumat';
            $hari[6] = 'Sabtu';
            $hari[7] = 'Minggu';
        }
        else {
            $hari[1] = 'Senin';
            $hari[2] = 'Selasa';
            $hari[3] = 'Rabu';
            $hari[4] = 'Kamis';
            $hari[5] = 'Jumat';
            $hari[6] = 'Sabtu';
            $hari[7] = 'Minggu';
        }
        
        return $hari;
    }
}

if(!function_exists('indoDay'))
{
    function indoDay($nhari,$full=true) {
        $hari = arrayDay();
        
        return $hari[$nhari];
    }
}

if(!function_exists('arrayMonth'))
{
    // nama bulan di bahasa indonesia
    function arrayMonth($full=true) {
        $bulan = array();
        
        if($full) {
            $bulan[1] = 'Januari';
            $bulan[2] = 'Pebruari';
            $bulan[3] = 'Maret';
            $bulan[4] = 'April';
            $bulan[5] = 'Mei';
            $bulan[6] = 'Juni';
            $bulan[7] = 'Juli';
            $bulan[8] = 'Agustus';
            $bulan[9] = 'September';
            $bulan[10] = 'Oktober';
            $bulan[11] = 'Nopember';
            $bulan[12] = 'Desember';
        }
        else {
            $bulan[1] = 'Jan';
            $bulan[2] = 'Peb';
            $bulan[3] = 'Mar';
            $bulan[4] = 'Apr';
            $bulan[5] = 'Mei';
            $bulan[6] = 'Jun';
            $bulan[7] = 'Jul';
            $bulan[8] = 'Agu';
            $bulan[9] = 'Sep';
            $bulan[10] = 'Okt';
            $bulan[11] = 'Nop';
            $bulan[12] = 'Des';
        }
        
        return $bulan;
    }
}

if(!function_exists('indoMonth'))
{
    function indoMonth($nbulan,$full=true) {
        $bulan = arrayMonth($full);
        
        return $bulan[(int)$nbulan];
    }
}

if(!function_exists('formatDateInd'))
{
    // mengubah format tanggal dari yyyy-mm-dd menjadi format indonesia
    function formatDateInd($ymd,$full=true,$dmy=false,$delim='-',$intd=true) {
        if($ymd == '')
            return '';
        if($ymd == 'null')
            return 'null';
        
        list($d,$m,$y) = splitDate($ymd,$dmy,$delim);
        if($intd)
            $d = (int)$d;
        
        return $d.' '.indoMonth($m,$full).' '.$y;
    }
}

if(!function_exists('hariIni'))
{
    function hariIni(){
        $hari = date ("D");

        switch($hari){
            case 'Sun':
                $hari_ini = "Minggu";
            break;

            case 'Mon':         
                $hari_ini = "Senin";
            break;

            case 'Tue':
                $hari_ini = "Selasa";
            break;

            case 'Wed':
                $hari_ini = "Rabu";
            break;

            case 'Thu':
                $hari_ini = "Kamis";
            break;

            case 'Fri':
                $hari_ini = "Jumat";
            break;

            case 'Sat':
                $hari_ini = "Sabtu";
            break;
            
            default:
                $hari_ini = "Tidak di ketahui";     
            break;
        }

        return $hari_ini;

    }
}

if(!function_exists('bulanIni'))
{
    function bulanIni(){
        $bulan = date ("m");

        switch($bulan){
            case '1':
                $bulan_ini = "Januari";
            break;

            case '2':         
                $bulan_ini = "Februari";
            break;

            case '3':
                $bulan_ini = "Maret";
            break;

            case '4':
                $bulan_ini = "April";
            break;

            case '5':
                $bulan_ini = "Mei";
            break;

            case '6':
                $bulan_ini = "Juni";
            break;

            case '7':
                $bulan_ini = "Juli";
            break;

            case '8':
                $bulan_ini = "Agustus";
            break;

            case '9':
                $bulan_ini = "September";
            break;

            case '10':
                $bulan_ini = "Oktober";
            break;

            case '11':
                $bulan_ini = "November";
            break;

            case '12':
                $bulan_ini = "Desember";
            break;
            
            default:
                $bulan_ini = "Tidak di ketahui";     
            break;
        }

        return $bulan_ini;

    }
}

if(!function_exists('tanggalIndo'))
{
    function tanggalIndo($tanggal, $cetak_hari = false)
    {
        $hari = array ( 1 =>    'Senin',
                    'Selasa',
                    'Rabu',
                    'Kamis',
                    'Jumat',
                    'Sabtu',
                    'Minggu'
                );
                
        $bulan = array (1 =>   'Januari',
                    'Februari',
                    'Maret',
                    'April',
                    'Mei',
                    'Juni',
                    'Juli',
                    'Agustus',
                    'September',
                    'Oktober',
                    'November',
                    'Desember'
                );
        $split    = explode('-', $tanggal);
        $tgl_indo = $split[2] . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[0];
        
        if ($cetak_hari) {
            $num = date('N', strtotime($tanggal));
            return $hari[$num] . ', ' . $tgl_indo;
        }
        return $tgl_indo;
    }
}

if(!function_exists('tanggalIndoLengkap'))
{
    function tanggalIndoLengkap($tanggal){
        $bulan = array (
            1 =>   'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        );
        $pecahkan = explode('-', $tanggal);
        
        // variabel pecahkan 0 = tanggal
        // variabel pecahkan 1 = bulan
        // variabel pecahkan 2 = tahun
        
        return $pecahkan[0] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[2];
        // return $bulan[ (int)$pecahkan[1] ] . '/' . $pecahkan[0] . '/' . $pecahkan[2];
    }
}

if(!function_exists('penyebut'))
{
    function penyebut($nilai) {
        $nilai = abs($nilai);
        $huruf = array("", "Satu", "Dua", "Tiga", "Empat", "Lima", "Enam", "Tujuh", "Delapan", "Sembilan", "Sepuluh", "Sebelas");
        $temp = "";
        if ($nilai < 12) {
            $temp = " ". $huruf[$nilai];
        } else if ($nilai <20) {
            $temp = penyebut($nilai - 10). " Belas";
        } else if ($nilai < 100) {
            $temp = penyebut($nilai/10)." Puluh". penyebut($nilai % 10);
        } else if ($nilai < 200) {
            $temp = " Seratus" . penyebut($nilai - 100);
        } else if ($nilai < 1000) {
            $temp = penyebut($nilai/100) . " Ratus" . penyebut($nilai % 100);
        } else if ($nilai < 2000) {
            $temp = " seribu" . penyebut($nilai - 1000);
        } else if ($nilai < 1000000) {
            $temp = penyebut($nilai/1000) . " Ribu" . penyebut($nilai % 1000);
        } else if ($nilai < 1000000000) {
            $temp = penyebut($nilai/1000000) . " Juta" . penyebut($nilai % 1000000);
        } else if ($nilai < 1000000000000) {
            $temp = penyebut($nilai/1000000000) . " Milyar" . penyebut(fmod($nilai,1000000000));
        } else if ($nilai < 1000000000000000) {
            $temp = penyebut($nilai/1000000000000) . " Trilyun" . penyebut(fmod($nilai,1000000000000));
        }     
        return $temp;
    }
}

if(!function_exists('terbilang'))
{
    function terbilang($nilai) {
        if($nilai<0) {
            $hasil = "minus ". trim(penyebut($nilai));
        } else {
            $hasil = trim(penyebut($nilai));
        }           
        return $hasil;
    }
}

if(!function_exists('tglAja'))
{
    //Fungsi ambil tanggal aja
    function tglAja($tgl_a){
        $tanggal = substr($tgl_a,8,2);
        return $tanggal;  
    }
}

if(!function_exists('blnAja'))
{
    //Fungsi Ambil bulan aja
    function blnAja($bulan_a){
        $bulan = getBulan(substr($bulan_a,5,2));
        return $bulan;  
    }
}

if(!function_exists('thnAja'))
{
    //Fungsi Ambil tahun aja
    function thn_aja($thn){
        $tahun = substr($thn,0,4);
        return $tahun;  
    }
}    

if(!function_exists('tglIndoTerbilang'))
{
    //Fungsi konversi tanggal bulan dan tahun ke dalam bahasa indonesia
    function tglIndoTerbilang($tgl){
        $tanggal = substr($tgl,8,2);
        $bulan = getBulan(substr($tgl,5,2));
        $tahun = substr($tgl,0,4);
        
        return $tanggal.' '.$bulan.' '.$tahun;  
    }
}     
        
?>