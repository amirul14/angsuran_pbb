<?php
	// fungsi format string
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );

	class CStr {

		// mengubah format bilangan
		function formatNumber($num,$dec=0) {
			return number_format($num,$dec,',','.');
		}
		
	}