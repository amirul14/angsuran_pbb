<?php

$nopel = '';
$nop = '';
$kec = '';
$kel = '';
$blok = '';
$urut = '';
$jns = '';
$thnpajak = '';
$pokok = '';
$jtempo = '';
$adj = '';
$denda = '';
$nama = '';
$alamat = '';
$nik = '';
$telp = '';
$jmlangsuran = '';
$tglpengajuan = '';
$ket = '';

$namawp = '';
$alamatwp = '';
$alamatno = '';
// var_dump($angsuranpbbInfo);die();
if(!empty($address))
{
    foreach ($address as $add) {
        $namawp = $add->D_WP_NAMA;
        $alamatwp = $add->D_OP_JLN;
        $alamatno = $add->D_OP_JLNO;
    }
}

if(!empty($angsuranpbbInfo))
{
    foreach ($angsuranpbbInfo as $af)
    {
        $nopel = $af->NOPEL;
        $nop = $af->USER_ID;
        $kec = $af->KEC;
        $kel = $af->KEL;
        $blok = $af->BLOK;
        $urut = $af->URUT;
        $jns = $af->JNS;
        $thnpajak = $af->TAHUN_PAJAK;
        $pokok = $af->POKOK;
        $jtempo = $af->JTEMPO;
        $adj = $af->ADJ;
        $denda = $af->DENDA_DISETUJUI;
        $nama = $af->NAMA;
        $alamat = $af->ALAMAT;
        $nik = $af->NIK;
        $telp = $af->TELP;
        $jmlangsuran = $af->JML_ANGSURAN;
        $tglpengajuan = $af->TGL_PENGAJUAN;
        $ket = $af->KET;
    }
}

$tot_tunggakan = $pokok + $adj + $denda;

?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-clipboard"></i> Angsuran PBB
        <small>Tambah / Sunting Angsuran</small>
      </h1>
    </section>
    
    <section class="content">
    
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
                
                
                
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Masukkan Detail Angsuran</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    
                    <form role="form" action="<?php echo base_url() ?>editAngsuran" method="post" id="editAngsuran" role="form">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="nop">Nomor Objek Pajak</label>
                                        <input type="text" class="form-control" id="nop" placeholder="Nomor Objek Pajak" name="nop" value="35.78.<?php echo $kec; ?>.<?php echo $kel; ?>.<?php echo $blok; ?>.<?php echo $urut; ?>.<?php echo $jns; ?>" maxlength="128" readonly>
                                        <input type="hidden" value="<?php echo $nopel; ?>" name="nopel" id="nopel" />    
                                    </div>
                                </div>
                                    
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="thnpajak">Tahun Pajak</label>
                                        <input type="text" class="form-control" id="thnpajak" placeholder="Tahun Pajak" name="thnpajak" value="<?php echo $thnpajak; ?>" maxlength="128" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="namawp">Nama Wajib Pajak</label>
                                        <input type="text" class="form-control" id="namawp" name="namawp" value="<?php echo $namawp; ?>" maxlength="128" readonly>
                                    </div>
                                </div>
                                    
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="alamatwp">Alamat Wajib Pajak</label>
                                        <input type="text" class="form-control" id="alamatwp" name="alamatwp" value="<?php echo $alamatwp; ?> <?php echo $alamatno; ?>" maxlength="128" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="pokok">Pokok Terutang</label>
                                        <input type="text" class="form-control text-right" id="pokok" placeholder="pokok" name="pokok" value="<?php echo formatNumber($pokok); ?>" maxlength="50" readonly>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="jtempo">Jatuh Tempo Ketetapan</label>
                                        <input type="text" class="form-control" id="jtempo" placeholder="Jatuh Tempo Ketetapan" name="jtempo" value="<?php echo date_format(date_create($jtempo),'j F Y'); ?>" maxlength="50" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="adj">Adjusment</label>
                                        <input type="text" class="form-control text-right" id="adj" placeholder="Adjusment" name="adj" value="<?php echo formatNumber($adj); ?>" maxlength="50" readonly>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="denda">Denda</label>
                                        <input type="text" class="form-control text-right" id="denda" placeholder="Denda" name="denda" value="<?php echo formatNumber($denda); ?>" maxlength="50" readonly>
                                    </div>
                                </div>    
                            </div>

                            <hr />

                            <div class="row">
                                <div class="col-md-6">
                                    &nbsp;
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="tot_tunggakan">Total Tunggakan (Otomatis oleh Sistem)</label>
                                        <span style="font-weight: bold;"><input type="text" class="form-control text-right" id="tot_tunggakan" name="tot_tunggakan" value="<?php echo formatNumber($tot_tunggakan); ?>" readonly></span>
                                    </div>
                                </div>
                            </div>

                            <hr />

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="nama">Nama Pemohon</label>
                                        <input type="text" class="form-control" id="nama" placeholder="Nama Pemohon" name="nama" value="<?php echo $nama; ?>" maxlength="128">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="alamat">Alamat Pemohon</label>
                                        <textarea class="form-control" id="alamat" name="alamat"><?php echo $alamat; ?></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="nik">Nomor Induk Kependudukan (NIK)</label>
                                        <input type="text" class="form-control" id="nik" placeholder="Nomor Induk Kependudukan (NIK)" name="nik" value="<?php echo $nik; ?>" maxlength="128">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="telp">Nomor Telepon</label>
                                        <input type="text" class="form-control" id="telp" placeholder="Nomor Telepon" name="telp" value="<?php echo $telp; ?>" maxlength="12">
                                    </div>
                                </div>    
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="tglpengajuan">Tanggal Pengajuan</label>
                                        <input type="date" class="form-control" id="tglpengajuan" placeholder="Tanggal Pengajuan" name="tglpengajuan" value="<?php echo date_format(date_create($tglpengajuan),'Y-m-d'); ?>">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="jmlangsuran">Jumlah Pengajuan Angsuran</label>
                                        <input type="text" class="form-control text-right" id="jmlangsuran" placeholder="Jumlah Pengajuan Angsuran" name="jmlangsuran" value="<?php echo $jmlangsuran; ?>" maxlength="20">
                                    </div>
                                </div>    
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="ket">Keterangan</label>
                                        <textarea class="form-control" id="ket" name="ket"><?php echo $ket; ?></textarea>
                                    </div>
                                </div>  
                            </div>
                        </div><!-- /.box-body -->

                        <div id="insert-form" class="box-body">
                            <button type="button" id="btn-tambah-form">Tambah Data Angsuran</button>
                            <?php foreach ($angsuranpbbDetail as $key => $val) { ?>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group" style="margin-bottom: 0px">
                                            <input type="text" class="form-control" id="angke" name="angke[]" placeholder="Angsuran Ke" value="<?php echo $val->ANGS_KE; ?>">
                                            <input type="hidden" value="<?php echo $nopel; ?>" name="nopel" id="nopel" />
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group" style="margin-bottom: 0px">
                                            <input type="text" class="form-control text-right prc" id="pokok_det" name="pokok_det[]" placeholder="Pokok Angsuran" value="<?php echo $val->POKOK_ANGS; ?>" onkeyup="tot_amount()">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group" style="margin-bottom: 0px">
                                            <input type="text" class="form-control text-right prc" id="sanksi_det" name="sanksi_det[]" placeholder="Sanksi Angsuran" value="<?php echo $val->SANKSI_ANGS; ?>"  onkeyup="tot_amount()">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group" style="margin-bottom: 0px">
                                            <input type="date" class="form-control" id="tglbayar" name="tglbayar[]" placeholder="Tanggal Bayar" value="<?php echo date_format(date_create($val->TGL_JANJI_BYR),'Y-m-d'); ?>">
                                        </div>
                                    </div>
                                </div>

                                <hr>

                                
                            <?php }
                            ?>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                &nbsp;
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="tot_angsuran">Total Perhitungan Angsuran (Otomatis oleh Sistem)</label>
                                    <span style="font-weight: bold;"><input type="text" class="form-control text-right" id="tot_angsuran" name="tot_angsuran" readonly></span>
                                </div>
                            </div>
                        </div>

                        <hr />
    
                        <div class="box-footer">
                            <input type="submit" class="btn btn-primary" value="Submit" />
                            <input type="reset" class="btn btn-default" value="Reset" />
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-8">
                <?php
                    $this->load->helper('form');
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error'); ?>                    
                </div>
                <?php } ?>
                <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>
                
                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                    </div>
                </div>
            </div>
        </div>    
    </section>
</div>

<script src="<?php echo base_url(); ?>assets/js/editOldAngsuran.js" type="text/javascript"></script>

<script>
$(document).ready(function(){ // Ketika halaman sudah diload dan siap    
// $(document).on("change", ".prc", function() {
    //     // console.log($(this));
    //     alert('Hokeh');
    //     var sum = 0;
    //     $(".prc").each(function(){
    //         sum += +$(this).val();
    //     });
    //     $(".result").val(sum);
    // });

    // var $pokok = $('#pokok_det'),
    //     $sanksi = $('#sanksi_det'),
    //     $tot_angsuran = $('#tot_angsuran');

    // $sanksi.on('keypress', function(e) {
    //     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)){
    //         return false;
    //     }

    //     $tot_angsuran.val(
    //         Math.round( ($pokok.val() - (($sanksi.val() / 100) * $pokok.val()))* 100 ) / 100
    //     );
        
    // });

    $("#btn-tambah-form").click(function(){ // Ketika tombol Tambah Data Form di klik      
        var jumlah = parseInt($("#jumlah-form").val()); // Ambil jumlah data form pada textbox jumlah-form      
        var nextform = jumlah + 1; // Tambah 1 untuk jumlah form nya            
        
        // Kita akan menambahkan form dengan menggunakan append      
        // pada sebuah tag div yg kita beri id insert-form      
    
        $("#insert-form").append(
            "<div class='row'>" +        
                "<div class='col-md-3'>" +
                    "<div class='form-group' style='margin-bottom: 0px'>" +
                        "<input type='text' class='form-control' id='angke' name='angke[]'' placeholder='Angsuran Ke'>" +
                    "</div>" +
                "</div>" +
                "<div class='col-md-3'>" +
                    "<div class='form-group' style='margin-bottom: 0px'>" +
                        "<input type='text' class='form-control text-right prc' id='pokok_det' name='pokok_det[]'' placeholder='Pokok Angsuran' value='0' onkeyup='tot_amount()'>" +
                    "</div>" +
                "</div>" +
                "<div class='col-md-3'>" +
                    "<div class='form-group' style='margin-bottom: 0px'>" +
                        "<input type='text' class='form-control text-right prc' id='sanksi_det' name='sanksi_det[]'' placeholder='Sanksi Angsuran' value='0' onkeyup='tot_amount()'>" +
                    "</div>" +
                "</div>" +
                "<div class='col-md-3'>" +
                    "<div class='form-group' style='margin-bottom: 0px'>" +
                        "<input type='date' class='form-control' id='tglbayar' name='tglbayar[]'' placeholder='Tanggal Bayar'>" +
                    "</div>" +
                "</div>" +
            "</div>" +        
            "<br><br>");            
    
        $("#jumlah-form").val(nextform); // Ubah value textbox jumlah-form dengan variabel nextform    

        tot_amount();
    });

    // Buat fungsi untuk mereset form ke semula    
    // $("#btn-reset-form").click(function(){      
    //     $("#insert-form").html(""); // Kita kosongkan isi dari div insert-form      
    //     $("#jumlah-form").val("1"); // Ubah kembali value jumlah form menjadi 1    
    // });  
});
</script>