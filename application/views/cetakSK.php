<?php

$nopel = '';
$nop = '';
$kec = '';
$kel = '';
$blok = '';
$urut = '';
$jns = '';
$thnpajak = '';
$pokok = '';
$jtempo = '';
$adj = '';
$denda = '';
$nama = '';
$alamat = '';
$nik = '';
$telp = '';
$jmlangsuran = '';
$tglpengajuan = '';
$ket = '';

$namawp = '';
$alamatwp = '';
$alamatno = '';

$angsuran_ke = '';
$pokok_angs = '';
$sanksi_angs = '';
$tgljanjibyr = '';
// var_dump($angsuranpbbInfo);die();
if(!empty(address))
{
    foreach ($address as $add) {
        $namawp = $add->D_WP_NAMA;
        $alamatwp = $add->D_OP_JLN;
        $alamatno = $add->D_OP_JLNO;
    }
}

if(!empty($angsuranpbbInfo))
{
    foreach ($angsuranpbbInfo as $af)
    {
        $nopel = $af->NOPEL;
        $nop = $af->USER_ID;
        $kec = $af->KEC;
        $kel = $af->KEL;
        $blok = $af->BLOK;
        $urut = $af->URUT;
        $jns = $af->JNS;
        $thnpajak = $af->TAHUN_PAJAK;
        $pokok = $af->POKOK;
        $jtempo = $af->JTEMPO;
        $adj = $af->ADJ;
        $denda = $af->DENDA_DISETUJUI;
        $nama = $af->NAMA;
        $alamat = $af->ALAMAT;
        $nik = $af->NIK;
        $telp = $af->TELP;
        $jmlangsuran = $af->JML_ANGSURAN;
        $tglpengajuan = $af->TGL_PENGAJUAN;
        $ket = $af->KET;
    }
}

if(!empty($angsuranpbbDetail))
{
    foreach ($angsuranpbbDetail as $ad) {
        $angsuran_ke = $ad->ANGS_KE;
        $pokok_angs = $ad->POKOK_ANGS;
        $sanksi_angs = $ad->SANKSI_ANGS;
        $tgljanjibyr = $ad->TGL_JANJI_BYR;
    }
}

$tanggalpengajuan = date("d n Y", strtotime($tglpengajuan));

$total_tunggakan = (float)$pokok + (float)$denda + (float)$adj;
$total_denda = (float)$denda + (float)$adj;
$hari_ini = date("D");
       
        /* -------------------------------- Halaman Pertama SK Angsuran -------------------------------- */

        $pdf = new FPDF('P','mm',array(215,330));
        $pdf->setTopMargin(20);
        $pdf->setLeftMargin(15);
        // membuat halaman baru
        $pdf->AddPage();
        // setting jenis font yang akan digunakan
        $pdf->SetFont('Arial','B',12);
        // mencetak string 
        $pdf->Image('assets/images/pemkot-sby.jpg',30,20,-2000);
        $pdf->Cell(240,3,'PEMERINTAH KOTA SURABAYA',0,1,'C');
        $pdf->Ln(3);
        $pdf->SetFont('Arial','B',18);
        $pdf->Cell(240,3,'BADAN PENGELOLAAN KEUANGAN',0,1,'C');
        $pdf->Ln(5);
        $pdf->SetFont('Arial','B',18);
        $pdf->Cell(230,3,'DAN PAJAK DAERAH',0,1,'C');
        $pdf->Ln(5);
        $pdf->SetFont('Arial','',9);
        $pdf->Cell(240,3,'Jalan Jimerto Nomor 25 - 27 Surabaya 60272',0,1,'C');
        $pdf->Ln(2);
        $pdf->SetFont('Arial','',9);
        $pdf->Cell(240,1,'Telepon (031) 5312144, ext. 137,328 Faksimile (031) 5321703',0,1,'C');
        $pdf->Ln(10);

        $pdf->SetFont('Arial','B',12);
        $pdf->Cell(200,1,'SURAT KEPUTUSAN',0,1,'C');
        $pdf->Ln(4);
        $pdf->SetFont('Arial','B',12);
        $pdf->Cell(200,1,'KEPALA BADAN PENGELOLAAN KEUANGAN DAN PAJAK DAERAH',0,1,'C');
        $pdf->Ln(4);
        $pdf->SetFont('Arial','B',12);
        $pdf->Cell(200,1,'KOTA SURABAYA',0,1,'C');
        $pdf->Ln(4);
        $pdf->SetFont('Arial','',12);
        $pdf->Cell(200,1,'Nomor : 973/.......06.5.1.02/436.8.2/2020',0,1,'C');
        $pdf->Ln(10);
        $pdf->SetFont('Arial','',12);
        $pdf->Cell(200,1,'Tentang : ',0,1,'C');
        $pdf->Ln(10);
        $pdf->SetFont('Arial','B',12);
        $pdf->Cell(200,1,'PERSETUJUAN ANGSURAN PEMBAYARAN PAJAK BUMI DAN BANGUNAN PERKOTAAN',0,1,'C');
        $pdf->Ln(4);
        $pdf->SetFont('Arial','B',12);
        $pdf->Cell(200,1,'KURANG BAYAR ATAS SPPT',0,1,'C');
        $pdf->Ln(10);

        $pdf->SetFont('Arial','',12);
        $pdf->Cell(40,6,'Menimbang');
        $pdf->Cell(5,6,':');
        $pdf->Cell(5,6,'a.');
        $pdf->MultiCell(140,6,'Hasil Pemeriksaan Kantor dan Lapangan perlu ditetapkan dengan Keputusan Kepala Badan Pengelolaan Keuangan dan Pajak Daerah tentang persetujuan angsuran pembayaran pajak');
        $pdf->Ln(3);
        $pdf->SetFont('Arial','',12);
        $pdf->Cell(45,6,'');
        $pdf->Cell(5,6,'b.');
        $pdf->MultiCell(140,6,'Berdasarkan permohonan angsuran dari Wajib Pajak '.$nama.' pada Tanggal '. $tanggalpengajuan);
        // $pdf->MultiCell(140,6,'Berdasarkan permohonan angsuran dari Wajib Pajak '.$nama.' pada Tanggal '. tanggalIndoLengkap($tanggalpengajuan));
        $pdf->Ln(3);
        $pdf->SetFont('Arial','',12);
        $pdf->Cell(45,6,'');
        $pdf->Cell(5,6,'c.');
        $pdf->MultiCell(140,6,'Berdasarkan pertimbangan sebagaimana dimaksud dalam huruf a dan b, maka perlu menetapkan Surat Keputusan Kepala Badan Pengelolaan Keuangan dan Pajak Daerah Kota Surabaya tentang Persetujuan Angsuran Pembayaran Pajak Bumi dan Bangunan Perkotaan Tahun Pajak '.$thnpajak);
        $pdf->Ln(5);

        $pdf->SetFont('Arial','',12);
        $pdf->Cell(40,6,'Mengingat');
        $pdf->Cell(5,6,':');
        $pdf->Cell(5,6,'1.');
        $pdf->MultiCell(140,6,'Undang-undang Nomor 28 Tahun 2009 tentang Pajak Daerah dan Retribusi Daerah (Lembaran Negara Tahun 2009 Nomor 130 Tambahan Lembaran Negara Tahun 2009 Nomor 5049);');
        $pdf->Ln(3);
        $pdf->SetFont('Arial','',12);
        $pdf->Cell(45,6,'');
        $pdf->Cell(5,6,'2.');
        $pdf->MultiCell(140,6,'Kepmendagri Nomor 43 Tahun 1999 tentang Sistem dan Prosedur Administrasi Pajak Daerah, Retribusi Daerah dan Penerimaan Pendapatan Lain-Lain;');
        $pdf->Ln(3);
        $pdf->SetFont('Arial','',12);
        $pdf->Cell(45,6,'');
        $pdf->Cell(5,6,'3.');
        $pdf->MultiCell(140,6,'Peraturan Daerah Kota Surabaya Nomor 10 Tahun 2010 tentang Pajak Bumi dan Bangunan Perkotaan (Lembaran Daerah Kota Surabaya Tahun 2010 Nomor 10 Tambahan Lembaran Daerah Kota Surabaya Nomor 8);');
        $pdf->Ln(3);
        $pdf->SetFont('Arial','',12);
        $pdf->Cell(45,6,'');
        $pdf->Cell(5,6,'4.');
        $pdf->MultiCell(140,6,'Peraturan Walikota Surabaya Nomor 70 Tahun 2016 tentang Rincian Tugas dan Fungsi Badan Pengelolaan Keuangan dan Pajak Daerah Kota Surabaya (Berita Daerah Kota Surabaya Tahun 2016 Nomor 75).');
        $pdf->Ln(3);
        
        
        $pdf->Ln(10);
        $pdf->SetFont('Arial','B',12);
        $pdf->Cell(200,1,'MEMUTUSKAN',0,1,'C');

        $pdf->Ln(7);
        $pdf->SetFont('Arial','B',12);
        $pdf->Cell(40,6,'Menimbang');
        $pdf->Cell(5,6,':');
        $pdf->MultiCell(140,6,'KEPUTUSAN KEPALA BADAN PENGELOLAAN KEUANGAN DAN PAJAK DAERAH TENTANG PERSETUJUAN ANGSURAN PEMBAYARAN PAJAK BUMI DAN BANGUNAN PERKOTAAN KURANG BAYAR ATAS SPPT');

        /* -------------------------------- Akhir Halaman Pertama SK Angsuran -------------------------------- */


        /* -------------------------------- Halaman Kedua SK Angsuran -------------------------------- */

        $pdf->setTopMargin(20);
        $pdf->setLeftMargin(15);
        // membuat halaman baru
        $pdf->AddPage();
        // setting jenis font yang akan digunakan
        $pdf->SetFont('Arial','B',12);
        // mencetak string
        $pdf->SetFont('Arial','',12);
        $pdf->Cell(30,3,'PERTAMA');
        $pdf->Cell(5,3,':');
        $pdf->Cell(50,3,'Nama Wajib Pajak');
        $pdf->Cell(5,3,':');
        $pdf->Cell(50,3, $namawp);
        $pdf->Ln(6);
        $pdf->SetFont('Arial','',12);
        $pdf->Cell(35,3,'');
        $pdf->Cell(50,3,'Alamat');
        $pdf->Cell(5,3,':');
        $pdf->Cell(50,3, $alamatwp.' '.$alamatno);
        $pdf->Ln(6);
        $pdf->SetFont('Arial','',12);
        $pdf->Cell(35,3,'');
        $pdf->Cell(50,3,'NPWPD / NOP');
        $pdf->Cell(5,3,':');
        $pdf->Cell(50,3, '35.78.'.$kec.'.'.$kel.'.'.$blok.'.'.$urut.'.'.$jns);
        $pdf->Ln(6);
        $pdf->SetFont('Arial','',12);
        $pdf->Cell(35,3,'');
        $pdf->Cell(50,3,'Nama Pemohon');
        $pdf->Cell(5,3,':');
        $pdf->Cell(50,3, $nama);
        $pdf->Ln(6);
        $pdf->SetFont('Arial','',12);
        $pdf->Cell(35,3,'');
        $pdf->Cell(50,3,'Alamat Pemohon');
        $pdf->Cell(5,3,':');
        $pdf->Cell(50,3, $alamat);
        $pdf->Ln(6);
        $pdf->SetFont('Arial','',12);
        $pdf->Cell(35,3,'');
        $pdf->Cell(50,3,'Pajak Terhutang');
        $pdf->Cell(5,3,':');
        $pdf->Cell(50,3, 'Rp. '.formatNumber($total_tunggakan));
        $pdf->Ln(6);
        $pdf->SetFont('Arial','',12);
        $pdf->Cell(35,6,'');
        $pdf->Cell(50,6,'Terbilang');
        $pdf->Cell(5,6,':');
        $pdf->MultiCell(100,6, terbilang($total_tunggakan).' Rupiah');
        $pdf->Ln(6);
        $pdf->SetFont('Arial','',12);
        $pdf->Cell(35,3,'');
        $pdf->Cell(50,3,'Tahun Pajak');
        $pdf->Cell(5,3,':');
        $pdf->Cell(50,3, $thnpajak.' Kurang Bayar Atas SPPT');
        $pdf->Ln(10);

        $pdf->SetFont('Arial','',12);
        $pdf->Cell(30,6,'KEDUA');
        $pdf->Cell(5,6,':');
        $pdf->MultiCell(150,6,'Menyetujui (Seluruhnya, Sebagian, Menolak)* dilakukan Pembayaran Angsuran Pajak Bumi dan Bangunan Perkotaan sebanyak '.$jmlangsuran.' ('.terbilang($jmlangsuran).') kali;');
        $pdf->Ln(7);

        $pdf->SetFont('Arial','',12);
        $pdf->Cell(30,6,'KETIGA');
        $pdf->Cell(5,6,':');
        $pdf->MultiCell(150,6,'Dalam hal wajib pajak pada diktum PERTAMA tidak melakukan pembayaran sesuai dengan surat perjanjian angsuran, maka dapat dikenakan sanksi sesuai dengan peraturan perundang - undangan yang berlaku;');
        $pdf->Ln(7);

        $pdf->SetFont('Arial','',12);
        $pdf->Cell(30,6,'KEEMPAT');
        $pdf->Cell(5,6,':');
        $pdf->MultiCell(150,6,'Apabila dikemudian hari terdapat kekeliruan dalam Surat Keputusan ini akan ditinjau kembali dan dibetulkan sesuai dengan ketentuan yang berlaku;');
        $pdf->Ln(7);

        $pdf->SetFont('Arial','',12);
        $pdf->Cell(30,6,'KELIMA');
        $pdf->Cell(5,6,':');
        $pdf->MultiCell(150,6,'Keputusan ini berlaku sejak tanggal ditetapkan.');
        $pdf->Ln(20);

        $pdf->SetFont('Arial','',12);
        $pdf->Cell(100,3,'');
        $pdf->Cell(35,3,'Ditetapkan di');
        $pdf->Cell(5,3,':');
        $pdf->Cell(30,3,'Surabaya');
        $pdf->Ln(6);
        $pdf->SetFont('Arial','',12);
        $pdf->Cell(100,3,'');
        $pdf->Cell(35,3,'Pada Tanggal');
        $pdf->Cell(5,3,':');
        $pdf->Cell(30,3, $tanggalpengajuan);
        $pdf->Ln(20);

        $pdf->SetFont('Arial','',12);
        $pdf->Cell(110,3,'');
        $pdf->Cell(40,3,'KEPALA BADAN,');
        $pdf->Ln(6);
        $pdf->Image('assets/images/YUSRON.jpg',130,204,-350);
        $pdf->Ln(30);
        $pdf->SetFont('Arial','BU',12);
        $pdf->Cell(100,3,'');
        $pdf->Cell(35,3,'Yusron Sumartono,S.E.,M.M.');
        $pdf->Ln(5);
        $pdf->SetFont('Arial','',12);
        $pdf->Cell(108,3,'');
        $pdf->Cell(40,3,'Pembina Utama Muda');
        $pdf->Ln(5);
        $pdf->SetFont('Arial','',12);
        $pdf->Cell(103,3,'');
        $pdf->Cell(40,3,'NIP 196603221987031001');
        $pdf->Ln(15);
        $pdf->SetFont('Arial','',12);
        $pdf->Cell(30,3,'Tembusan');
        $pdf->Cell(4,3,':');
        $pdf->Ln(5);
        $pdf->SetFont('Arial','',12);
        $pdf->Cell(30,3,'Yth. Sdr.1. Kepala Bidang Pendataan dan Penetapan Pajak Daerah');
        $pdf->Ln(5);
        $pdf->SetFont('Arial','',12);
        $pdf->Cell(16,3,'');
        $pdf->Cell(40,3,'2. Kepala UPTB Pelayanan Pajak Surabaya');
        $pdf->Ln(5);

        /* -------------------------------- Akhir Halaman Kedua SK Angsuran -------------------------------- */



        /* -------------------------------- Halaman Ketiga SK Angsuran -------------------------------- */

        $pdf->setTopMargin(20);
        $pdf->setLeftMargin(10);
        // membuat halaman baru
        $pdf->AddPage();
        // mencetak string
        $pdf->SetFont('Arial','B',16);
        $pdf->Cell(50,3,'');
        $pdf->Cell(30,3,'SURAT PERJANJIAN ANGSURAN');
        $pdf->Ln(10);
        $pdf->SetFont('Arial','',12);
        $pdf->Cell(55,3,'');
        $pdf->Cell(50,3,'Nomor : 973/.....06.5.1.02/436.8.2/2020');
        $pdf->Ln(10);
        $pdf->SetFont('Arial','',11);
        $pdf->MultiCell(0,5,'Berdasarkan Permohonan Angsuran Wajib Pajak Tanggal '.$tanggalpengajuan.' Tentang Pembayaran Angsuran Pajak Bumi dan Bangunan Perkotaan Pada Hari ini '.hariIni().' Tanggal '. terbilang(date("d")) . ' Bulan ' . bulanIni() . ' Tahun '. terbilang(date("Y")) .' yang bertanda tangan di bawah ini:');
        $pdf->Ln(8);

        $pdf->SetFont('Arial','',12);
        $pdf->Cell(5,3,'');
        $pdf->Cell(30,3,'Bertindak dan atas nama diri Sendiri / Kuasa dari '.$namawp.' Selaku ..........');
        $pdf->Ln(8);

        $pdf->SetFont('Arial','',12);
        $pdf->Cell(5,3,'');
        $pdf->Cell(30,3,'Nama');
        $pdf->Cell(5,3,':');
        $pdf->Cell(30,3, $nama);
        $pdf->Ln(6);
        $pdf->SetFont('Arial','',12);
        $pdf->Cell(5,3,'');
        $pdf->Cell(30,3,'Alamat');
        $pdf->Cell(5,3,':');
        $pdf->Cell(30,3, $alamat);
        $pdf->Ln(6);
        $pdf->SetFont('Arial','',12);
        $pdf->Cell(5,3,'');
        $pdf->Cell(30,3,'NIK');
        $pdf->Cell(5,3,':');
        $pdf->Cell(30,3, $nik);
        $pdf->Ln(6);
        $pdf->SetFont('Arial','',12);
        $pdf->Cell(5,3,'');
        $pdf->Cell(30,3,'Telepon');
        $pdf->Cell(5,3,':');
        $pdf->Cell(30,3, $telp);
        $pdf->Ln(8);

        $pdf->SetFont('Arial','',12);
        $pdf->Cell(5,3,'');
        $pdf->Cell(30,3,'Selaku Kepala Bidang Penagihan dan Pengurangan Pajak Daerah');
        $pdf->Ln(8);

        $pdf->SetFont('Arial','',12);
        $pdf->Cell(5,3,'');
        $pdf->Cell(30,3,'Nama');
        $pdf->Cell(5,3,':');
        $pdf->Cell(30,3, 'Agung Supriyo Wibowo, S.E.,A.k.,M.A.');
        $pdf->Ln(6);
        $pdf->SetFont('Arial','',12);
        $pdf->Cell(5,3,'');
        $pdf->Cell(30,3,'Jabatan');
        $pdf->Cell(5,3,':');
        $pdf->Cell(30,3, 'Kepala Bidang Penagihan dan Pengurangan Pajak Daerah');
        $pdf->Ln(6);
        $pdf->SetFont('Arial','',12);
        $pdf->Cell(5,3,'');
        $pdf->Cell(30,3,'NIK');
        $pdf->Cell(5,3,':');
        $pdf->Cell(30,3, '196806121996021000');
        $pdf->Ln(8);

        $pdf->SetFont('Arial','',12);
        $pdf->Cell(5,6,'1.');
        $pdf->MultiCell(190,6,'Kedua belah pihak sepakat bahwa Angsuran Pembayaran Atas SPPT Tahun Pajak '.$thnpajak.' dengan N.O.P : 35.78.'.$kec.'.'.$kel.'.'.$blok.'.'.$urut.'.'.$jns.' Sebesar Rp. '.formatNumber($total_tunggakan).' ('.terbilang($total_tunggakan).' Rupiah)');
        $pdf->Ln(5);
        $pdf->MultiCell(0,5,'Ketetapan akan diangsur sebanyak '.$jmlangsuran.' ('.terbilang($jmlangsuran).') kali, Dengan perincian sebagai berikut:');
        $pdf->Ln(5);

        $pdf->SetFont('Arial','B',10);
        $pdf->SetFillColor(255,150,100);
        $pdf->Cell(105,5,' ','LTR',0,'L',0);   // empty cell with left,top, and right borders
        $pdf->Cell(60,5,'Jumlah','TR',0,'C',0);
        $pdf->Cell(30,5,'Total','TR',0,'C',0);   // empty cell with top, and right borders
        $pdf->Ln();
        $pdf->Cell(105,5,'Tunggakan Atas SPPT PBB','LR',0,'C',0);
        $pdf->Cell(30,5,'Pokok','TR',0,'C',0);
        $pdf->Cell(30,5,'Sanksi','TR',0,'C',0);
        $pdf->Cell(30,5,'Tunggakan','R',0,'C',0);   // empty cell with top, and right borders
        $pdf->Ln();
        $pdf->Cell(105,5,' ','LBR',0,'L',0);   // empty cell with left,top, and right borders
        $pdf->Cell(30,5,'Rp. '.formatNumber($pokok),'TBR',0,'R',0);
        $pdf->Cell(30,5,'Rp. '.formatNumber($total_denda),'TBR',0,'R',0);
        $pdf->Cell(30,5,'Rp. '.formatNumber($total_tunggakan),'TBR',0,'R',0);
        $pdf->Ln(7);        

        $pdf->SetFont('Arial','B',10);
        $pdf->Cell(7,5,'No','LTR',0,'C',0);
        $pdf->Cell(18,5,'Dasar','TR',0,'C',0);
        $pdf->Cell(20,5,'Tahun','TR',0,'C',0);
        $pdf->Cell(30,5,'Pokok','TR',0,'C',0);
        $pdf->Cell(30,5,'Sanksi','TR',0,'C',0);
        $pdf->Cell(30,5,'Total','TR',0,'C',0);
        $pdf->Cell(60,5,'Kesanggupan Pembayaran','TR',0,'C',0);
        $pdf->Ln();
        $pdf->Cell(7,5,'','LBR',0,'C',0);
        $pdf->Cell(18,5,'Setor','BR',0,'C',0);
        $pdf->Cell(20,5,'Pajak','BR',0,'C',0);
        $pdf->Cell(30,5,'','BR',0,'C',0);
        $pdf->Cell(30,5,'','BR',0,'C',0);
        $pdf->Cell(30,5,'','BR',0,'C',0);
        $pdf->Cell(30,5,'Angsuran','BTR',0,'C',0);
        $pdf->Cell(30,5,'Tangal Bayar','BTR',0,'C',0);
        $pdf->Ln();

        $pdf->AliasNbPages();
        $i=0;
        if(count($angsuranpbbDetail) > 0) {
                foreach ($angsuranpbbDetail as $key => $val) {
                        $i++;
                        $tot = (float)$val->POKOK_ANGS + (float)$val->SANKSI_ANGS;
                        $tot_pokok += (float)$val->POKOK_ANGS;
                        $tot_sanksi += (float)$val->SANKSI_ANGS;
                        $grand_total = $tot_pokok + $tot_sanksi;

                        $pdf->Cell(7,5,$i,'LBTR',0,'C');
                        $pdf->Cell(18,5,'SPPT','LBTR',0,'C',0);
                        $pdf->Cell(20,5,$thnpajak,'LBTR',0,'C',0);
                        $pdf->Cell(30,5,'Rp. '.formatNumber($val->POKOK_ANGS),'LBTR',0,'R',0);
                        $pdf->Cell(30,5,'Rp. '.formatNumber($val->SANKSI_ANGS),'LBTR',0,'R',0);
                        $pdf->Cell(30,5,'Rp. '.formatNumber($tot),'LBTR',0,'R',0);
                        $pdf->Cell(30,5,'Ke '.$val->ANGS_KE,'LBTR',0,'C',0);
                        $pdf->Cell(30,5,$val->TGL_JANJI_BYR,'LBTR',0,'C',0);
                        $pdf->Ln();
                }
        }
        
        $pdf->Cell(45,5,'Total','LBTR',0,'C',0);
        $pdf->Cell(30,5,'Rp. '.formatNumber($tot_pokok),'LBTR',0,'R',0);
        $pdf->Cell(30,5,'Rp. '.formatNumber($tot_sanksi),'LBTR',0,'R',0);
        $pdf->Cell(30,5,'Rp. '.formatNumber($grand_total),'LBTR',0,'R',0);
        $pdf->Cell(60,5,'','LBTR',0,'C',0);
        $pdf->Ln();
        $pdf->Cell(45,5,'Terbilang','LBTR',0,'C',0);
        $pdf->SetFont('Arial','BI',10);
        $pdf->MultiCell(150,5,'('.terbilang($grand_total).' Rupiah)','LBR','L',0);

        /* -------------------------------- Akhir Halaman Ketiga SK Angsuran -------------------------------- */


        /* -------------------------------- Halaman Keempat SK Angsuran -------------------------------- */

        $pdf->setTopMargin(20);
        $pdf->setLeftMargin(10);
        // membuat halaman baru
        $pdf->AddPage();

        $pdf->Ln(5);
        $pdf->SetFont('Arial','',12);
        $pdf->Cell(5,6,'2.');
        $pdf->MultiCell(190,6,'Jika Perjanjian Angsuran pada sub 1 diatas selaku Pihak I tidak penuhi maka penagihan dengan Surat Paksa tanpa pemberitahuan terlebih dahulu, hal ini sesuai dengan Peraturan Daerah Kota Surabaya No. 10 Tahun 2010 pada pasal 18 ayat (4) yang menyebutkan bahwa : "Surat Paksa diterbitkan apabila Wajib Pajak dan/ atau Penanggung Pajak tidak memenuhi ketentuan sebagaimana tercantum dalam Keputusan Persetujuan Angsuran atau penundaan pembayaran pajak".');
        $pdf->Ln(5);
        $pdf->SetFont('Arial','',12);
        $pdf->Cell(5,6,'3.');
        $pdf->MultiCell(190,6,'Selain Pembayaran angsuran Pihak I diwajibkan untuk membayar Pajak Bumi dan Bangunan Perkotaan tahun berjalan sesuai penetapan SPPT PBB dan tidak melebihi dari jatuh tempo pembayaran dan penyetoran pajak terutang sebagaimana dalam SPPT PBB.');
        $pdf->Ln(5);
        $pdf->SetFont('Arial','',12);
        $pdf->Cell(5,6,'4.');
        $pdf->MultiCell(190,6,'Jika Pihak I terlambat dalam pembayaran angsuran atau tidak sesuai dengan tanggal yang telah disepakati bersama, dalam perjanjian angsuran maka akan dikenakan bunga sebesar 2% perbulan dari pokok pajak yang terhutang, Jika sampai dua kali angsuran terlambat membayar angsuran akan dilakukan penempelan stiker peringatan.');
        $pdf->Ln(5);
        $pdf->SetFont('Arial','',12);
        $pdf->Cell(5,6,'5.');
        $pdf->MultiCell(190,6,'Sanksi administratif berupa bunga 2% paling lama 24 bulan atas terlambat dibayar setiap bulan belum termasuk dalam angsuran masih berjalan dan akan ditagihkan melalui Surat Penagihan.');
        $pdf->Ln(10);

        $pdf->SetFont('Arial','',12);
        $pdf->Cell(25,6,'');
        $pdf->Cell(115,6,'Mengetahui dan Menyetujui,');
        $pdf->Cell(75,6,'Wajib Pajak');
        $pdf->Ln(5);
        $pdf->SetFont('Arial','',12);
        $pdf->Cell(10,6,'');
        $pdf->Cell(115,6,'Kepala Bidang Penagihan dan Pengurangan');
        $pdf->Ln(35);
        $pdf->SetFont('Arial','BU',12);
        $pdf->Cell(15,6,'');
        $pdf->Cell(116,6,'Agung Supriyo Wibowo, S.E.,Ak.,M.A.');
        $pdf->Cell(5,6,'');
        $pdf->Cell(75,6, $nama);
        $pdf->Ln(5);
        $pdf->SetFont('Arial','',12);
        $pdf->Cell(44,6,'');
        $pdf->Cell(115,6,'Pembina');
        $pdf->Ln(5);
        $pdf->SetFont('Arial','',12);
        $pdf->Cell(27,6,'');
        $pdf->Cell(115,6,'NIP 196806121996021003');

        $pdf->Output();

?>        