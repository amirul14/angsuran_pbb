<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-clipboard"></i> Angsuran PBB
        <small>Tambah, Sunting, Hapus</small>
      </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12 text-right">
                <div class="form-group">
                    <a class="btn btn-primary" href="<?php echo base_url(); ?>addNewAngPBB"><i class="fa fa-plus"></i> Tambah Baru</a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Data Angsuran PBB</h3>
                    <div class="box-tools">
                        <form action="<?php echo base_url() ?>angsuranpbbListing" method="POST" id="searchList">
                            <div class="input-group">
                              <input type="text" name="searchText" value="<?php echo $searchText; ?>" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Search"/>
                              <div class="input-group-btn">
                                <button class="btn btn-sm btn-default searchList"><i class="fa fa-search"></i></button>
                              </div>
                            </div>
                        </form>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                  <table class="table table-hover">
                    <tr>
                      <th>No. Pelayanan</th>
                      <th>NOP.</th>
                      <th>Nama</th>
                      <th>Alamat</th>
                      <th>Tgl. Pengajuan</th>
                      <th>Tahun Pajak</th>
                      <th class="text-center">Aksi</th>
                    </tr>
                    <?php
                    if(!empty($angsuranpbbRecords))
                    {
                        foreach($angsuranpbbRecords as $record)
                        {
                    ?>
                    <tr>
                      <td><?php echo $record->NOPEL ?></td>
                      <td>35.78.<?php echo $record->KEC ?>.<?php echo $record->KEL ?>.<?php echo $record->BLOK ?>.<?php echo $record->URUT ?>.<?php echo $record->JNS ?></td>
                      <td><?php echo $record->NAMA ?></td>
                      <td><?php echo $record->ALAMAT ?></td>
                      <td><?php echo $record->TGL_PENGAJUAN ?></td>
                      <td><?php echo $record->TAHUN_PAJAK ?></td>
                      <td class="text-center">
                          <a class="btn btn-sm btn-success" href="<?php echo base_url().'editOldAngsuran/'.$record->NOPEL; ?>"><i class="fa fa-pencil"></i></a>
                          <!-- <a class="btn btn-sm btn-success" href="<?php// echo base_url().'simpanDetAngsuran/'.$record->NOPEL; ?>"><i class="fa fa-edit"></i></a> -->
                          <a class="btn btn-sm btn-primary" href="<?php echo base_url().'lihatDetAngsuran/'.$record->NOPEL; ?>"><i class="fa fa-eye"></i></a>
                          <!-- <a class="btn btn-sm btn-warning" href="<?php// echo base_url().'cetakSK/'.$record->NOPEL; ?>"><i class="fa fa-print"></i></a> -->
                      </td>
                    </tr>
                    <?php
                        }
                    }
                    ?>
                  </table>
                  
                </div><!-- /.box-body -->
                <div class="box-footer clearfix">
                    <?php echo $this->pagination->create_links(); ?>
                </div>
              </div><!-- /.box -->
            </div>
        </div>

        <!-- Modal -->
        <div id="myModal" class="modal fade" role="dialog">
          <div class="modal-dialog">
            <!-- konten modal-->
            <div class="modal-content">
              <!-- heading modal -->
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Data Angsuran PBB</h4>
              </div>
              <!-- body modal -->
              <div class="modal-body">
                  <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nop">Nomor Objek Pajak</label>
                            <input type="text" class="form-control-plaintext" id="nop" name="nop" readonly="true" value="<?php echo $nop; ?>">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="thnpajak">Tahun Pajak</label>
                            <input type="text" class="form-control-plaintext" id="thnpajak" name="thnpajak" readonly="true" value="<?php echo $thnpajak; ?>">
                        </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="pokok">Pokok Terutang</label>
                            <input type="text" class="form-control-plaintext" id="pokok" name="pokok" readonly="" value="<?php echo $pokok; ?>">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="jtempo">Jatuh Tempo Ketetapan</label>
                            <input type="text" class="form-control-plaintext" id="jtempo" name="jtempo" readonly="" value="<?php echo $jtempo; ?>">
                        </div>
                    </div>
                  </div>
              </div>
              <!-- footer modal -->
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
              </div>
            </div>
          </div>
        </div>
    </section>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/common.js" charset="utf-8"></script>
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('ul.pagination li a').click(function (e) {
            e.preventDefault();            
            var link = jQuery(this).get(0).href;            
            var value = link.substring(link.lastIndexOf('/') + 1);
            jQuery("#searchList").attr("action", baseURL + "angsuranpbbListing/" + value);
            jQuery("#searchList").submit();
        });
    });
</script>
<script>
    $(function () {
    $('#btn-edit').click(function (e) {
            e.preventDefault();
            $('#myModal').modal({
                backdrop: 'static',
                show: true
            });
            id = $(this).data('id');
            // mengambil nilai data-id yang di click
            $.ajax({
                url: 'profile/edit/' + id,
                success: function (data) {
                    $("input[name='id']").val(data.id);
                    $("input[name='nama']").val(data.nama);
                    $("textarea[name='alamat']").val(data.alamat);
                }
            });
       });
}
</script>