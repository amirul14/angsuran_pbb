<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-clipboard"></i> Angsuran PBB
        <small>Tambah / Sunting Angsuran PBB</small>
      </h1>
    </section>
    
    <section class="content">
    
        <div class="row">
            <!-- left column -->
            <div class="col-md-10">
              <!-- general form elements -->
                
                
                
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Masukkan Detail Angsuran PBB</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    
                    <form role="form" id="addAngsuran" action="<?php echo base_url() ?>addNewAngsuran" method="post" role="form">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="nop">Nomor Objek Pajak</label>
                                        <input type="text" class="form-control required" id="nop" name="nop" maxlength="128" class="uang">
                                    </div>
                                </div>
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="thnpajak">Tahun Pajak</label>
                                        <input type="text" class="form-control required" id="thnpajak" name="thnpajak" maxlength="128">
                                        <button class="btn btn-warning" type="button" onclick="goCariNOP()">Cari NOP</button>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="namawp">Nama Wajib Pajak</label>
                                        <input type="text" class="form-control" readonly="" id="namawp"  name="namawp" maxlength="50">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="alamatwp">Alamat Wajib Pajak</label>
                                        <textarea class="form-control" readonly="" id="alamatwp" name="alamatwp"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="pokok">Pokok Terutang</label>
                                        <input type="text" class="form-control digits" readonly="" id="pokok" name="pokok" maxlength="50">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="jtempo">Jatuh Tempo Ketetapan</label>
                                        <input type="text" class="form-control" readonly="" id="jtempo" name="jtempo">
                                    </div>
                                </div>    
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="adj">Adjusment</label>
                                        <input type="text" class="form-control digits" readonly="" id="adj" name="adj" maxlength="50">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="denda">Denda</label>
                                        <input type="text" class="form-control digits" readonly="" id="denda" name="denda" maxlength="50">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="nama">Nama Pemohon</label>
                                        <input type="text" class="form-control required" id="nama" name="nama" maxlength="128">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="alamat">Alamat Pemohon</label>
                                        <textarea class="form-control required" id="alamat" name="alamat"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="nik">Nomor Induk Kependudukan (NIK)</label>
                                        <input type="text" class="form-control required" id="nik" name="nik" maxlength="128">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="telp">Nomor Telepon</label>
                                        <input type="text" class="form-control required" id="telp" name="telp" maxlength="12">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="tglpengajuan">Tanggal Pengajuan</label>
                                        <input type="date" class="form-control required" id="tglpengajuan" name="tglpengajuan">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="jmlangsuran">Jumlah Pengajuan Angsuran</label>
                                        <input type="text" class="form-control required digits" id="jmlangsuran" name="jmlangsuran" maxlength="20">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="ket">Keterangan</label>
                                        <textarea class="form-control" id="ket" name="ket"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div><!-- /.box-body -->
    
                        <div class="box-footer">
                            <input type="submit" class="btn btn-primary" value="Submit" />
                            <input type="reset" class="btn btn-default" value="Reset" />
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-4">
                <?php
                    $this->load->helper('form');
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error'); ?>                    
                </div>
                <?php } ?>
                <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>
                
                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                    </div>
                </div>
            </div>
        </div>    
    </section>
    
</div>
<script src="<?php echo base_url(); ?>assets/js/addAngsuran.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/jquery/jquery-3.4.1.min.js"></script>
<script src="<?php echo base_url(); ?>assets/jquery/jquery.mask.min.js"></script>

<script type="text/javascript">
    // $(document).ready(function(){
    //     // $(":input").inputmask();

    //     // $("#nop").inputmask({"mask": "(999) 999-9999"});
    //     // Format NOP.
    //     $( '.nop_mask' ).mask('0000−0000−0000');

    //     $( '.uang' ).mask('000.000.000', {reverse: true});
    // });

    function goCariNOP() {
        var nop = $('#nop').val();
        var thnnop = $('#thnpajak').val();

        if (nop != "" && thnnop != "") {
            $.ajax({
                url: "<?php echo site_url('angsuranpbb/cariNOP');?>",
                type: "POST",
                data : {nop : nop, thnnop : thnnop},
                async : true,
                dataType : 'json',
                success: function(data)
                {
                    $.each(data, function(i, v){
                        $('#'+i).val(v);    
                    });
                }          
            });

        } else {
            alert('Masukkan NOP dan Tahun Pajak...');
        }

        
    }

    /* Fungsi formatRupiah */
    function formatRupiah(angka, prefix){
        var number_string = angka.replace(/[^,\d]/g, '').toString(),
        split           = number_string.split(','),
        sisa            = split[0].length % 3,
        rupiah          = split[0].substr(0, sisa),
        ribuan          = split[0].substr(sisa).match(/\d{3}/gi);

        // tambahkan titik jika yang di input sudah menjadi angka ribuan
        if(ribuan){
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }

        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
    }

    
</script>