<?php

$nopel = '';
$nop = '';
$kec = '';
$kel = '';
$blok = '';
$urut = '';
$jns = '';
$thnpajak = '';
$pokok = '';
$jtempo = '';
$adj = '';
$denda = '';
$nama = '';
$alamat = '';
$nik = '';
$telp = '';
$jmlangsuran = '';
$tglpengajuan = '';
$ket = '';

$namawp = '';
$alamatwp = '';
$alamatno = '';
// var_dump($angsuranpbbInfo);die();
if(!empty($address))
{
    foreach ($address as $add) {
        $namawp = $add->D_WP_NAMA;
        $alamatwp = $add->D_OP_JLN;
        $alamatno = $add->D_OP_JLNO;
    }
}

if(!empty($angsuranpbbInfo))
{
    foreach ($angsuranpbbInfo as $af)
    {
        $nopel = $af->NOPEL;
        $nop = $af->USER_ID;
        $kec = $af->KEC;
        $kel = $af->KEL;
        $blok = $af->BLOK;
        $urut = $af->URUT;
        $jns = $af->JNS;
        $thnpajak = $af->TAHUN_PAJAK;
        $pokok = $af->POKOK;
        $jtempo = $af->JTEMPO;
        $adj = $af->ADJ;
        $denda = $af->DENDA_DISETUJUI;
        $nama = $af->NAMA;
        $alamat = $af->ALAMAT;
        $nik = $af->NIK;
        $telp = $af->TELP;
        $jmlangsuran = $af->JML_ANGSURAN;
        $tglpengajuan = $af->TGL_PENGAJUAN;
        $ket = $af->KET;
    }
}

$tot_tunggakan = $pokok + $adj + $denda;

?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i> Detail Angsuran PBB
        <small>Tambah / Sunting Detail Angsuran</small>
      </h1>
    </section>
    
    <section class="content">
    
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
                
                
                
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Detail Angsuran</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    
                    <!-- <form role="form" action="<?php echo base_url() ?>editDetailAngsuran" method="post" id="editDetailAngsuran" role="form"> -->
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="nop">Nomor Objek Pajak</label>
                                        <input type="text" class="form-control" id="nop" placeholder="Nomor Objek Pajak" name="nop" value="35.78.<?php echo $kec; ?>.<?php echo $kel; ?>.<?php echo $blok; ?>.<?php echo $urut; ?>.<?php echo $jns; ?>" maxlength="128" readonly>
                                        <!-- <input type="hidden" value="<?php// echo $nopel; ?>" name="nopel" id="nopel" />     -->
                                    </div>
                                </div>
                                    
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="thnpajak">Tahun Pajak</label>
                                        <input type="text" class="form-control" id="thnpajak" placeholder="Tahun Pajak" name="thnpajak" value="<?php echo $thnpajak; ?>" maxlength="128" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="namawp">Nama Wajib Pajak</label>
                                        <input type="text" class="form-control" id="namawp" name="namawp" value="<?php echo $namawp; ?>" maxlength="128" readonly>
                                    </div>
                                </div>
                                    
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="alamatwp">Alamat Wajib Pajak</label>
                                        <input type="text" class="form-control" id="alamatwp" name="alamatwp" value="<?php echo $alamatwp; ?> <?php echo $alamatno; ?>" maxlength="128" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="pokok">Pokok Terutang</label>
                                        <input type="text" class="form-control" id="pokok" placeholder="pokok" name="pokok" value="<?php echo formatNumber($pokok); ?>" maxlength="50" readonly>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="jtempo">Jatuh Tempo Ketetapan</label>
                                        <input type="text" class="form-control" id="jtempo" placeholder="Jatuh Tempo Ketetapan" name="jtempo" value="<?php echo date_format(date_create($jtempo),'j F Y'); ?>" maxlength="50" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="adj">Adjusment</label>
                                        <input type="text" class="form-control" id="adj" placeholder="Adjusment" name="adj" value="<?php echo formatNumber($adj); ?>" maxlength="50" readonly>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="denda">Denda</label>
                                        <input type="text" class="form-control" id="denda" placeholder="Denda" name="denda" value="<?php echo formatNumber($denda); ?>" maxlength="50" readonly>
                                    </div>
                                </div>    
                            </div>

                            <hr />

                            <div class="row">
                                <div class="col-md-6">
                                    &nbsp;
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="tot_tunggakan">Total Tunggakan (Otomatis oleh Sistem)</label>
                                        <span style="font-weight: bold;"><input type="text" class="form-control text-right" id="tot_tunggakan" name="tot_tunggakan" value="<?php echo formatNumber($tot_tunggakan); ?>" readonly></span>
                                    </div>
                                </div>
                            </div>

                            <hr />

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="nama">Nama Pemohon</label>
                                        <input type="text" class="form-control" id="nama" placeholder="Nama Pemohon" name="nama" value="<?php echo $nama; ?>" maxlength="128" readonly>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="alamat">Alamat Pemohon</label>
                                        <textarea class="form-control" id="alamat" name="alamat" readonly><?php echo $alamat; ?></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="nik">Nomor Induk Kependudukan (NIK)</label>
                                        <input type="text" class="form-control" id="nik" placeholder="Nomor Induk Kependudukan (NIK)" name="nik" value="<?php echo $nik; ?>" maxlength="128" readonly>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="telp">Nomor Telepon</label>
                                        <input type="text" class="form-control" id="telp" placeholder="Nomor Telepon" name="telp" value="<?php echo $telp; ?>" maxlength="12" readonly>
                                    </div>
                                </div>    
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="tglpengajuan">Tanggal Pengajuan</label>
                                        <input type="text" class="form-control" id="tglpengajuan" name="tglpengajuan" value="<?php echo date_format(date_create($tglpengajuan),'j F Y'); ?>" readonly>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="jmlangsuran">Jumlah Pengajuan Angsuran</label>
                                        <input type="text" class="form-control" id="jmlangsuran" placeholder="Jumlah Pengajuan Angsuran" name="jmlangsuran" value="<?php echo $jmlangsuran; ?>" maxlength="20" readonly>
                                    </div>
                                </div>    
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="ket">Keterangan</label>
                                        <textarea class="form-control" id="ket" name="ket" readonly><?php echo $ket; ?></textarea>
                                    </div>
                                </div>  
                            </div>
                        </div><!-- /.box-body -->
                        
                    <!-- </form> -->

                    <form role="form" action="<?php echo base_url() ?>simpanDetailAngsuran" method="post" id="simpanDetailAngsuran" role="form">
                        <div class="box-body">
                            <?php for ($i=0; $i < $jmlangsuran ; $i++) { ?>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group" style="margin-bottom: 0px">
                                            <input type="text" class="form-control" id="angke" name="angke[]" placeholder="Angsuran Ke" value="<?php echo $i+1; ?>">
                                            <input type="hidden" value="<?php echo $nopel; ?>" name="nopel" id="nopel" />
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group" style="margin-bottom: 0px">
                                            <input type="text" class="form-control" id="pokok_det" name="pokok_det[]" placeholder="Pokok Angsuran">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group" style="margin-bottom: 0px">
                                            <input type="text" class="form-control" id="sanksi_det" name="sanksi_det[]" placeholder="Sanksi Angsuran">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group" style="margin-bottom: 0px">
                                            <input type="date" class="form-control" id="tglbayar" name="tglbayar[]" placeholder="Tanggal Bayar">
                                        </div>
                                    </div>
                                </div>
                                <hr>
                            <?php }
                            ?>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                &nbsp;
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="tot_angsuran">Total Perhitungan Angsuran (Otomatis oleh Sistem)</label>
                                    <span style="font-weight: bold;"><input type="text" class="form-control text-right" id="tot_angsuran" name="tot_angsuran" readonly></span>
                                </div>
                            </div>
                        </div>

                        <hr />

                        <div class="box-footer">
                            <input type="submit" class="btn btn-primary" value="Submit" />
                            <input type="reset" class="btn btn-default" value="Reset" />
                        </div>
                    </form>
                </div>  
            </div>
            <div class="col-md-10">
                <?php
                    $this->load->helper('form');
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error'); ?>                    
                </div>
                <?php } ?>
                <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>
                
                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                    </div>
                </div>
            </div>
        </div>    
    </section>
</div>

<script src="<?php echo base_url(); ?>assets/js/simpanDetAngsuran.js" type="text/javascript"></script>

<script type="text/javascript">
        
    var rupiah = document.getElementById('pokok_det');
    rupiah.addEventListener('keyup', function(e){
        // tambahkan 'Rp.' pada saat form di ketik
        // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
        rupiah.value = formatRupiah(this.value, 'Rp. ');
    });

    /* Fungsi formatRupiah */
    function formatRupiah(angka, prefix){
        var number_string = angka.replace(/[^,\d]/g, '').toString(),
        split           = number_string.split(','),
        sisa            = split[0].length % 3,
        rupiah          = split[0].substr(0, sisa),
        ribuan          = split[0].substr(sisa).match(/\d{3}/gi);

        // tambahkan titik jika yang di input sudah menjadi angka ribuan
        if(ribuan){
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }

        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
    }


    var sanksi = document.getElementById('sanksi_det');
    sanksi.addEventListener('keyup', function(e){
        // tambahkan 'Rp.' pada saat form di ketik
        // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
        sanksi.value = formatRupiah(this.value, 'Rp. ');
    });

    /* Fungsi formatRupiah */
    function formatRupiah(angka, prefix){
        var number_string = angka.replace(/[^,\d]/g, '').toString(),
        split           = number_string.split(','),
        sisa            = split[0].length % 3,
        sanksi          = split[0].substr(0, sisa),
        ribuan          = split[0].substr(sisa).match(/\d{3}/gi);

        // tambahkan titik jika yang di input sudah menjadi angka ribuan
        if(ribuan){
            separator = sisa ? '.' : '';
            sanksi += separator + ribuan.join('.');
        }

        sanksi = split[1] != undefined ? sanksi + ',' + split[1] : sanksi;
        return prefix == undefined ? sanksi : (sanksi ? 'Rp. ' + sanksi : '');
    }
</script>