<?php
//include connection file 
// include_once("connection.php");
// include_once('libs/fpdf.php');

// $selected_tgl = $_POST['tglcetaktbl'];
// $selected_user = $_POST['jenisuser'];
// var_dump($selected_tgl);
$conn = oci_connect('pelayanan', 'pelayanan', '10.21.35.252/dbprmb');
if (!$conn) {
    $e = oci_error();
    trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
}

// $this->load->library('pdf');
require('application/third_party/fpdf181/fpdf.php');

class PDF extends FPDF
{
    // Page header
    function Header()
    {
        // Logo
        $this->Image('pemkot-sby-mobling.jpg',15,5,-2000);
        $this->SetFont('Arial','B',12);
        $this->SetFillColor(255,255,255);
        // Move to the right
        // $this->Cell(80);
        // Title
        $this->Cell(200,1,'Laporan Harian Mobil Keliling',0,0,'C');
        // Line break
        $this->Ln(5);
        // Sub Title
        $this->Cell(200,1,'Pajak Daerah',0,0,'C');
        // Line break
        $this->Ln(15);
    }

    // Page footer
    function Footer()
    {
        // Position at 1.5 cm from bottom
        $this->SetY(-15);
        // Arial italic 8
        $this->SetFont('Arial','I',8);
        // Page number
        $this->Cell(0,10,'Halaman '.$this->PageNo().'/{nb}',0,0,'C');
    }
}

function tgl_indo($selected_tgl){
    $bulan = array (
        1 =>   'Januari',
        'Februari',
        'Maret',
        'April',
        'Mei',
        'Juni',
        'Juli',
        'Agustus',
        'September',
        'Oktober',
        'November',
        'Desember'
    );
    $pecahkan = explode('-', $selected_tgl);
    
    // variabel pecahkan 0 = tanggal
    // variabel pecahkan 1 = bulan
    // variabel pecahkan 2 = tahun
 
    return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
}

$pdf->Output();
?>