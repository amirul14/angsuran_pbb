<?php

$nopel = '';
$nop = '';
$kec = '';
$kel = '';
$blok = '';
$urut = '';
$jns = '';
$thnpajak = '';
$pokok = '';
$jtempo = '';
$adj = '';
$denda = '';
$nama = '';
$alamat = '';
$nik = '';
$telp = '';
$jmlangsuran = '';
$tglpengajuan = '';
$ket = '';

$namawp = '';
$alamatwp = '';
$alamatno = '';

$angsuran_ke = '';
$pokok_angs = '';
$sanksi_angs = '';
$tgljanjibyr = '';
// var_dump($angsuranpbbInfo);die();
if(!empty(address))
{
    foreach ($address as $add) {
        $namawp = $add->D_WP_NAMA;
        $alamatwp = $add->D_OP_JLN;
        $alamatno = $add->D_OP_JLNO;
    }
}

if(!empty($angsuranpbbInfo))
{
    foreach ($angsuranpbbInfo as $af)
    {
        $nopel = $af->NOPEL;
        $nop = $af->USER_ID;
        $kec = $af->KEC;
        $kel = $af->KEL;
        $blok = $af->BLOK;
        $urut = $af->URUT;
        $jns = $af->JNS;
        $thnpajak = $af->TAHUN_PAJAK;
        $pokok = $af->POKOK;
        $jtempo = $af->JTEMPO;
        $adj = $af->ADJ;
        $denda = $af->DENDA_DISETUJUI;
        $nama = $af->NAMA;
        $alamat = $af->ALAMAT;
        $nik = $af->NIK;
        $telp = $af->TELP;
        $jmlangsuran = $af->JML_ANGSURAN;
        $tglpengajuan = $af->TGL_PENGAJUAN;
        $ket = $af->KET;
    }
}

if(!empty($angsuranpbbDetail))
{
    foreach ($angsuranpbbDetail as $ad) {
        $angsuran_ke = $ad->ANGS_KE;
        $pokok_angs = $ad->POKOK_ANGS;
        $sanksi_angs = $ad->SANKSI_ANGS;
        $tgljanjibyr = $ad->TGL_JANJI_BYR;
    }
}

$tot_tunggakan = $pokok + $adj + $denda;

?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-clipboard"></i> Detail Pengajuan Angsuran PBB
        <small>Informasi Detail Angsuran</small>
      </h1>
    </section>
    
    <section class="content">
    
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
                
                
                
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Detail Pengajuan Angsuran PBB</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    
                    
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="nop">Nomor Objek Pajak</label>
                                        <input type="text" class="form-control" id="nop" placeholder="Nomor Objek Pajak" name="nop" value="35.78.<?php echo $kec; ?>.<?php echo $kel; ?>.<?php echo $blok; ?>.<?php echo $urut; ?>.<?php echo $jns; ?>" maxlength="128" readonly>
                                        <input type="hidden" value="<?php echo $nopel; ?>" name="nopel" id="nopel" />
                                    </div>
                                </div>
                                    
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="thnpajak">Tahun Pajak</label>
                                        <input type="text" class="form-control" id="thnpajak" placeholder="Tahun Pajak" name="thnpajak" value="<?php echo $thnpajak; ?>" maxlength="128" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="namawp">Nama Wajib Pajak</label>
                                        <input type="text" class="form-control" id="namawp" name="namawp" value="<?php echo $namawp; ?>" maxlength="128" readonly>
                                    </div>
                                </div>
                                    
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="alamatwp">Alamat Wajib Pajak</label>
                                        <input type="text" class="form-control" id="alamatwp" name="alamatwp" value="<?php echo $alamatwp; ?> <?php echo $alamatno; ?>" maxlength="128" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="pokok">Pokok Terutang</label>
                                        <input type="text" class="form-control text-right" id="pokok" placeholder="pokok" name="pokok" value="<?php echo formatNumber($pokok); ?>" maxlength="50" readonly>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="jtempo">Jatuh Tempo Ketetapan</label>
                                        <input type="text" class="form-control" id="jtempo" placeholder="Jatuh Tempo Ketetapan" name="jtempo" value="<?php echo date_format(date_create($jtempo),'j F Y'); ?>" maxlength="50" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="adj">Adjusment</label>
                                        <input type="text" class="form-control text-right" id="adj" placeholder="Adjusment" name="adj" value="<?php echo formatNumber($adj); ?>" maxlength="50" readonly>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="denda">Denda</label>
                                        <input type="text" class="form-control text-right" id="denda" placeholder="Denda" name="denda" value="<?php echo formatNumber($denda); ?>" maxlength="50" readonly>
                                    </div>
                                </div>    
                            </div>

                            <hr />

                            <div class="row">
                                <div class="col-md-6">
                                    &nbsp;
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Total Tunggakan (Otomatis oleh Sistem)</label>
                                        <input type="text" class="form-control text-right" id="tot_tunggakan" name="tot_tunggakan" value="<?php echo formatNumber($tot_tunggakan); ?>" readonly>
                                    </div>
                                </div>
                            </div>

                            <hr />
                            
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="nama">Nama Pemohon</label>
                                        <input type="text" class="form-control" id="nama" placeholder="Nama Pemohon" name="nama" value="<?php echo $nama; ?>" maxlength="128" readonly>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="alamat">Alamat Pemohon</label>
                                        <textarea class="form-control" id="alamat" name="alamat" readonly><?php echo $alamat; ?></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="nik">Nomor Induk Kependudukan (NIK)</label>
                                        <input type="text" class="form-control" id="nik" placeholder="Nomor Induk Kependudukan (NIK)" name="nik" value="<?php echo $nik; ?>" maxlength="128" readonly>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="telp">Nomor Telepon</label>
                                        <input type="text" class="form-control" id="telp" placeholder="Nomor Telepon" name="telp" value="<?php echo $telp; ?>" maxlength="12" readonly>
                                    </div>
                                </div>    
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="tglpengajuan">Tanggal Pengajuan</label>
                                        <input type="text" class="form-control" id="tglpengajuan" name="tglpengajuan" value="<?php echo date_format(date_create($tglpengajuan),'j F Y'); ?>" readonly>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="jmlangsuran">Jumlah Pengajuan Angsuran</label>
                                        <input type="text" class="form-control text-right" id="jmlangsuran" placeholder="Jumlah Pengajuan Angsuran" name="jmlangsuran" value="<?php echo $jmlangsuran; ?>" maxlength="20" readonly>
                                    </div>
                                </div>    
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="ket">Keterangan</label>
                                        <textarea class="form-control" id="ket" name="ket" readonly><?php echo $ket; ?></textarea>
                                    </div>
                                </div>  
                            </div>
                        </div><!-- /.box-body -->
                    <hr />
                    <div class="clearfix"></div>
                        <div class="box-header">
                            <h4 class="box-title"><u>Detail Nominal Angsuran PBB</u></h4>
                        </div>

                    <div class="box-body table-responsive no-padding">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>No. </th>
                                        <th>Angsuran Ke</th>
                                        <th>Pokok Angsuran</th>
                                        <th>Sanksi Angsuran</th>
                                        <th>Tanggal Janji Bayar</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $i = 0;
                                        if(count($angsuranpbbDetail) > 0) {
                                            foreach ($angsuranpbbDetail as $key => $val) {
                                                $i++;
                                                $tot_pokok += (float)$val->POKOK_ANGS;
                                                $tot_sanksi += (float)$val->SANKSI_ANGS;
                                                $grand_total = $tot_pokok + $tot_sanksi;
                                    ?>
                                    <tr valign="top">
                                        <td><?= $i.'.' ?></td>
                                        <td align="center"><?php echo $val->ANGS_KE ?></td>
                                        <td align="right"><?php echo formatNumber($val->POKOK_ANGS) ?></td>
                                        <td align="right"><?php echo formatNumber($val->SANKSI_ANGS) ?></td>
                                        <td align="center"><?php echo $val->TGL_JANJI_BYR ?></td>
                                    </tr>
                                    <?      }
                                        } else { 
                                    ?>
                                    <tr><td colspan="5" align="center"><b>-- Data tidak ditemukan --</b></td></tr>
                                    <?
                                        }
                                    ?>
                                    <tr>
                                        <td colspan="2" align="center"><b>Total Angsuran</b>&nbsp;&nbsp;&nbsp;</td>
                                        <td align="right"><b><?php echo formatNumber($tot_pokok,2) ?></b></td>
                                        <td align="right"><b><?php echo formatNumber($tot_sanksi,2) ?></b></td>
                                        <td align="center"><b><?php echo formatNumber($grand_total,2) ?></b></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <form role="form" action="<?php echo base_url('cetakSK/'.$nopel) ?>" method="post" id="cetakSK" target="_blank">
                        <div class="box-footer">
                            <input type="submit" class="btn btn-primary" value="Cetak SK" />
                        </div>
                    </form>
                    <!-- <form id="form_items" name="form_items" target="_blank" action="<?php// echo base_url() ?>cetakSK ?>" method="post" enctype="multipart/form-data">
                        <input type="submit" value=" Cetak Laporan " class="btn btn-info pull-right"/>
                        
                        <input type="hidden" value="<?php// echo $nopel; ?>" name="nopel" id="nopel" />
                        <input type="hidden" name="tglcetaktbl" id="tglcetaktbl">
                    </form> -->

                </div>  
            </div>
            <div class="col-md-10">
                <?php
                    $this->load->helper('form');
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error'); ?>                    
                </div>
                <?php } ?>
                <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>
                
                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                    </div>
                </div>
            </div>
        </div>    
    </section>
</div>

<!-- <script src="<?php// echo base_url(); ?>assets/js/editDetailAngsuran.js" type="text/javascript"></script> -->

<script type="text/javascript">
        
    var rupiah = document.getElementById('pokok_det');
    rupiah.addEventListener('keyup', function(e){
        // tambahkan 'Rp.' pada saat form di ketik
        // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
        rupiah.value = formatRupiah(this.value, 'Rp. ');
    });

    /* Fungsi formatRupiah */
    function formatRupiah(angka, prefix){
        var number_string = angka.replace(/[^,\d]/g, '').toString(),
        split           = number_string.split(','),
        sisa            = split[0].length % 3,
        rupiah          = split[0].substr(0, sisa),
        ribuan          = split[0].substr(sisa).match(/\d{3}/gi);

        // tambahkan titik jika yang di input sudah menjadi angka ribuan
        if(ribuan){
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }

        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
    }


    var sanksi = document.getElementById('sanksi_det');
    sanksi.addEventListener('keyup', function(e){
        // tambahkan 'Rp.' pada saat form di ketik
        // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
        sanksi.value = formatRupiah(this.value, 'Rp. ');
    });

    /* Fungsi formatRupiah */
    function formatRupiah(angka, prefix){
        var number_string = angka.replace(/[^,\d]/g, '').toString(),
        split           = number_string.split(','),
        sisa            = split[0].length % 3,
        sanksi          = split[0].substr(0, sisa),
        ribuan          = split[0].substr(sisa).match(/\d{3}/gi);

        // tambahkan titik jika yang di input sudah menjadi angka ribuan
        if(ribuan){
            separator = sisa ? '.' : '';
            sanksi += separator + ribuan.join('.');
        }

        sanksi = split[1] != undefined ? sanksi + ',' + split[1] : sanksi;
        return prefix == undefined ? sanksi : (sanksi ? 'Rp. ' + sanksi : '');
    }

    /*  Fungsi untuk Cetak Laporan    */
    function formaction( str )
    {
        switch( str )
        {   
            case "sk_angsuran":
            document.form_items.action = 'rep_skangsuran.php';
            document.form_items.submit();
            break;
        }
    }
</script>