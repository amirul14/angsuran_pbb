<?php

$nopel = '';
$nop = '';
$thnpajak = '';
$pokok = '';
$jtempo = '';

if(!empty($angsuranpbbInfo))
{
    foreach ($angsuranpbbInfo as $af)
    {
        $nopel = $af->NOPEL;
        $nop = $af->USER_ID;
        $thnpajak = $af->TAHUN_PAJAK;
        $pokok = $af->POKOK;
        $jtempo = $af->JTEMPO;
    }
}    

?>


<!-- Start Modal -->
<div id="modalAngsuran" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
	<div class="modal-dialog">
		<!-- Modal content-->
      	<div class="modal-content">
      		<div class="modal-header">
      			<button type="button" class="close" data-dismiss="modal">&times;</button>
          		<h4 class="modal-title" id="modal-title">Data Angsuran PBB</h4>
      		</div>

      		<div class="modal-body">
      			<div class="box-body">
      				<div class="row">
      					<div class="col-md-6">
                            <div class="form-group">
                                <label for="nop">Nomor Objek Pajak</label>
                                <input type="text" class="form-control-plaintext" id="nop" name="nop" readonly="" value="<?php echo $nop; ?>">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="thnpajak">Tahun Pajak</label>
                                <input type="text" class="form-control-plaintext" id="thnpajak" name="thnpajak" readonly="" value="<?php echo $thnpajak; ?>">
                            </div>
                        </div>
      				</div>

      				<div class="row">
      					<div class="col-md-6">
                            <div class="form-group">
                                <label for="pokok">Pokok Terutang</label>
                                <input type="text" class="form-control-plaintext" id="pokok" name="pokok" readonly="" value="<?php echo $pokok; ?>">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="jtempo">Jatuh Tempo Ketetapan</label>
                                <input type="text" class="form-control-plaintext" id="jtempo" name="jtempo" readonly="" value="<?php echo $jtempo; ?>">
                            </div>
                        </div>
      				</div>
      			</div>
      		</div>
      	</div>
      	<!-- End Modal content-->
	</div>
</div>
<!-- End Modal -->  