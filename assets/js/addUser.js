/**
 * File : addUser.js
 * 
 * This file contain the validation of add user form
 * 
 * Using validation plugin : jquery.validate.js
 * 
 * @author Kishor Mali
 */

$(document).ready(function(){
	
	var addUserForm = $("#addUser");
	
	var validator = addUserForm.validate({
		
		rules:{
			fname :{ required : true },
			// email : { required : true, email : true, remote : { url : baseURL + "checkEmailExists", type :"post"} },
			password : { required : true },
			cpassword : {required : true, equalTo: "#password"},
			role : { required : true, selected : true}
		},
		messages:{
			fname :{ required : "Tidak boleh kosong" },
			// email : { required : "Tidak boleh kosong", email : "Please enter valid email address", remote : "Email already taken" },
			password : { required : "Tidak boleh kosong" },
			cpassword : {required : "Tidak boleh kosong", equalTo: "Password yang dimasukkan tidak sama" },
			role : { required : "Tidak boleh kosong", selected : "Silahkan pilih salah satu Role" }			
		}
	});
});
