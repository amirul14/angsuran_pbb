/**
 * File : editOldAngsuran.js 
 * 
 * This file contain the validation of edit user form
 * 
 * @author Amirul Mu'minin
 */
// $(function() {

	function numberWithSeparator(x) {
	    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
	}

	function tot_amount() {

		var sum = 0;

		$('.prc').each(function(){

			var num = $(this).val().replace(',','');

			if(num != 0){
				sum += parseFloat(num);
			}

		});

		$('#tot_angsuran').val(numberWithSeparator(sum));
		// $('#tot_angsuran').val(sum.toFixed(2));

	}

	// Keyup Handler

	// $('.prc').keyup(function(){

	// 	tot_amount();

	// });

	tot_amount();

	var editAngsuranForm = $("#editOldAngsuran");
	
	var validator = editAngsuranForm.validate({
		
		rules:{
			tot_angsuran : {equalTo: "#tot_tunggakan"}
		},
		messages:{
			tot_angsuran : {equalTo: "Nilai Total Angsuran tidak sesuai dengan Total Tunggakan" }
		}
	});

// });