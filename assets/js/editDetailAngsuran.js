/**
 * File : editUser.js 
 * 
 * This file contain the validation of edit user form
 * 
 * @author Kishor Mali
 */
$(document).ready(function(){
	
	var editDetailAngsuranForm = $("#editDetailAngsuran");
	
	// var validator = editAngsuranForm.validate({
		
	// 	rules:{
	// 		nop :{ required : true },
	// 		thnpajak : { required : true },
	// 		nama : { required : true },
	// 		alamat : {required : true },
	// 		nik : { required : true, digits : true },
	// 		telp : { required : true, digits : true },
	// 		tglpengajuan : { required : true },
	// 		jmlangsuran : { required : true, digits : true }
	// 	},
	// 	messages:{
	// 		nop :{ required : "NOP tidak boleh kosong" },
	// 		thnpajak : { required : "Tahun Pajak tidak boleh kosong" },
	// 		nama : { required : "Nama Pemohon Tidak boleh kosong" },
	// 		alamat : {required : "Alamat tidak boleh kosong" },
	// 		nik : { required : "NIK tidak boleh kosong", digits : "Inputan harus berupa angka" },
	// 		telp : { required : "Nomor Telpon tidak boleh kosong", digits : "Inputan harus berupa angka" },
	// 		tglpengajuan : {required : "Tanggal Pengajuan tidak boleh kosong" },
	// 		jmlangsuran : { required : "Jumlah Angsuran tidak boleh kosong", digits : "Inputan harus berupa angka" }
	// 	}
	// });
});

function tandaPemisahTitik(b){
	var _minus = false;
	if (b<0) _minus = true;
	b = b.toString();
	b=b.replace(".","");
	b=b.replace("-","");
	c = "";
	panjang = b.length;
	j = 0;
	for (i = panjang; i > 0; i--){
		 j = j + 1;
		 if (((j % 3) == 1) && (j != 1)){
		   c = b.substr(i-1,1) + "." + c;
		 } else {
		   c = b.substr(i-1,1) + c;
		 }
	}
	if (_minus) c = "-" + c ;
	return c;
}