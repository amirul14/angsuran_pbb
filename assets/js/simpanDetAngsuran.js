/**
 * File : simpanDetAngsuran.js 
 * 
 * This file contain the validation of edit user form
 * 
 * @author Amirul Mu'minin
 */

	function numberWithSeparator(x) {
	    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
	}

	function tot_amount() {

		var sum = 0;

		$('.prc').each(function(){

			var num = $(this).val().replace(',','');

			if(num != 0){
				sum += parseFloat(num);
			}

		});

		$('#tot_angsuran').val(numberWithSeparator(sum));

	}

	tot_amount();

	var simpanDetAngsuranForm = $("#simpanDetAngsuran");
	
	var validator = simpanDetAngsuranForm.validate({
		
		rules:{
			tot_angsuran : {equalTo: "#tot_tunggakan"}
		},
		messages:{
			tot_angsuran : {equalTo: "Nilai Total Angsuran tidak sesuai dengan Total Tunggakan" }
		}
	});