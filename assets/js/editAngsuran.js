/**
 * File : editUser.js 
 * 
 * This file contain the validation of edit user form
 * 
 * @author Kishor Mali
 */
$(document).ready(function(){
	
	var editAngsuranForm = $("#editAngsuran");
	
	var validator = editAngsuranForm.validate({
		
		rules:{
			nop :{ required : true },
			thnpajak : { required : true },
			nama : { required : true },
			alamat : {required : true },
			nik : { required : true, digits : true },
			telp : { required : true, digits : true },
			tglpengajuan : { required : true },
			jmlangsuran : { required : true, digits : true }
		},
		messages:{
			nop :{ required : "NOP tidak boleh kosong" },
			thnpajak : { required : "Tahun Pajak tidak boleh kosong" },
			nama : { required : "Nama Pemohon Tidak boleh kosong" },
			alamat : {required : "Alamat tidak boleh kosong" },
			nik : { required : "NIK tidak boleh kosong", digits : "Inputan harus berupa angka" },
			telp : { required : "Nomor Telpon tidak boleh kosong", digits : "Inputan harus berupa angka" },
			tglpengajuan : {required : "Tanggal Pengajuan tidak boleh kosong" },
			jmlangsuran : { required : "Jumlah Angsuran tidak boleh kosong", digits : "Inputan harus berupa angka" }
		}
	});
});